-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 24, 2019 at 10:51 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_inv`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) NOT NULL,
  `c_code` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `c_code`, `name`, `created_at`) VALUES
(10, 'CAT-0010', 'Polosan', '2019-07-07 03:53:23'),
(11, 'CAT-0011', 'Metalize', '2019-07-07 03:53:27');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(10) NOT NULL,
  `cs_code` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `cs_code`, `name`, `created_at`) VALUES
(12, 'SPN-C0012', 'Sarana Prima Nusantara', '2019-07-07 03:51:01'),
(13, 'S-C0013', 'Sindomas', '2019-07-07 03:51:06'),
(14, 'MGI-C0014', 'Modern Gravure Indotama', '2019-07-07 03:51:20'),
(15, 'M-C0015', 'Mayora', '2019-07-07 03:51:25');

-- --------------------------------------------------------

--
-- Table structure for table `incoming`
--

CREATE TABLE `incoming` (
  `incoming_id` int(10) NOT NULL,
  `inc_code` varchar(20) NOT NULL,
  `incoming` text NOT NULL,
  `inc_date` date DEFAULT NULL,
  `status` int(1) NOT NULL,
  `user_id` int(10) NOT NULL,
  `user_updated` int(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `incoming`
--

INSERT INTO `incoming` (`incoming_id`, `inc_code`, `incoming`, `inc_date`, `status`, `user_id`, `user_updated`, `created_at`) VALUES
(1, 'INC/070719/KT0001', 'a:2:{i:0;a:6:{s:6:\"i_code\";s:10:\"MP-MF-0006\";s:4:\"name\";s:13:\"Metalize Film\";s:6:\"s_code\";s:9:\"WMA-S0003\";s:3:\"qty\";s:2:\"40\";s:6:\"status\";i:0;s:5:\"stock\";i:940;}i:1;a:6:{s:6:\"i_code\";s:10:\"PP-PP-0007\";s:4:\"name\";s:3:\"P P\";s:6:\"s_code\";s:7:\"J-S0004\";s:3:\"qty\";s:2:\"90\";s:6:\"status\";i:0;s:5:\"stock\";i:1790;}}', '2019-07-07', 0, 1, 1, '2019-07-07 19:28:23'),
(2, 'INC/140819/KT0002', 'a:1:{i:0;a:6:{s:6:\"i_code\";s:10:\"MP-MF-0006\";s:4:\"name\";s:13:\"Metalize Film\";s:6:\"s_code\";s:9:\"WMA-S0003\";s:3:\"qty\";s:2:\"99\";s:6:\"status\";i:0;s:5:\"stock\";i:1039;}}', '2019-08-14', 0, 1, 1, '2019-07-07 19:28:33'),
(3, 'INC/070719/KT0003', 'a:1:{i:0;a:6:{s:6:\"i_code\";s:10:\"PP-PP-0007\";s:4:\"name\";s:3:\"P P\";s:6:\"s_code\";s:7:\"J-S0004\";s:3:\"qty\";s:3:\"600\";s:6:\"status\";i:0;s:5:\"stock\";i:1700;}}', '2019-07-07', 0, 1, 1, '2019-07-07 19:28:46'),
(5, 'INC/170719/KT0005', 'a:2:{i:0;a:6:{s:6:\"i_code\";s:10:\"MP-MF-0006\";s:4:\"name\";s:13:\"Metalize Film\";s:6:\"s_code\";s:9:\"WMA-S0003\";s:3:\"qty\";s:2:\"91\";s:6:\"status\";i:0;s:5:\"stock\";i:882;}i:1;a:6:{s:6:\"i_code\";s:10:\"PP-PP-0007\";s:4:\"name\";s:3:\"P P\";s:6:\"s_code\";s:7:\"J-S0004\";s:3:\"qty\";s:2:\"10\";s:6:\"status\";i:0;s:5:\"stock\";i:1800;}}', '2019-07-17', 0, 1, 1, '2019-07-17 10:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `item_id` int(10) NOT NULL,
  `i_code` varchar(20) NOT NULL,
  `t_code` varchar(20) NOT NULL,
  `c_code` varchar(20) NOT NULL,
  `s_code` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `qty` int(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `i_code`, `t_code`, `c_code`, `s_code`, `name`, `qty`, `created_at`) VALUES
(6, 'MP-MF-0006', 'M/P', 'CAT-0011', 'WMA-S0003', 'Metalize Film', 965, '2019-07-07 03:53:59'),
(7, 'PP-PP-0007', 'P/P', 'CAT-0010', 'J-S0004', 'P P', 1699, '2019-07-07 03:54:13');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `log_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `log` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`log_id`, `user_id`, `name`, `log`, `created_at`) VALUES
(1, 1, 'Arman Septian (IT)', '<b>Menghapus Pengguna</b> : Menghapus pengguna dengan Nama : Arman Septian', '2019-07-24 15:09:12'),
(2, 1, 'Arman Septian (IT)', '<b>Registrasi Pengguna Baru</b> : Mendaftarkan pengguna baru dengan email : septian.arman009@gmail.com \r\n            cek email untuk melihat password yang sudah dikirimkan', '2019-07-24 15:12:24'),
(3, 4, 'Arman Septian', '<b>Memperbarui Pengguna</b> : Memperbarui pengguna dengan email : idah.dpooh@gmail.com', '2019-07-24 15:44:10');

-- --------------------------------------------------------

--
-- Table structure for table `outgoing`
--

CREATE TABLE `outgoing` (
  `outgoing_id` int(10) NOT NULL,
  `otg_code` varchar(20) NOT NULL,
  `outgoing` text NOT NULL,
  `otg_date` date DEFAULT NULL,
  `status` int(1) NOT NULL,
  `user_id` int(10) NOT NULL,
  `user_updated` int(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outgoing`
--

INSERT INTO `outgoing` (`outgoing_id`, `otg_code`, `outgoing`, `otg_date`, `status`, `user_id`, `user_updated`, `created_at`) VALUES
(1, 'OTG/070719/KT0001', 'a:2:{i:0;a:7:{s:7:\"cs_code\";s:7:\"S-C0013\";s:6:\"i_code\";s:10:\"MP-MF-0006\";s:4:\"name\";s:13:\"Metalize Film\";s:6:\"s_code\";s:9:\"WMA-S0003\";s:3:\"qty\";s:2:\"70\";s:6:\"status\";i:0;s:5:\"stock\";s:4:\"1039\";}i:1;a:7:{s:7:\"cs_code\";s:9:\"SPN-C0012\";s:6:\"i_code\";s:10:\"MP-MF-0006\";s:4:\"name\";s:13:\"Metalize Film\";s:6:\"s_code\";s:9:\"WMA-S0003\";s:3:\"qty\";s:2:\"90\";s:6:\"status\";i:0;s:5:\"stock\";i:969;}}', '2019-07-07', 0, 1, 1, '2019-07-07 19:28:56'),
(2, 'OTG/080819/KT0002', 'a:1:{i:0;a:7:{s:7:\"cs_code\";s:9:\"MGI-C0014\";s:6:\"i_code\";s:10:\"MP-MF-0006\";s:4:\"name\";s:13:\"Metalize Film\";s:6:\"s_code\";s:9:\"WMA-S0003\";s:3:\"qty\";s:3:\"123\";s:6:\"status\";i:0;s:5:\"stock\";s:3:\"879\";}}', '2019-08-08', 0, 1, 1, '2019-07-07 19:29:18'),
(3, 'OTG/040919/KT0003', 'a:1:{i:0;a:7:{s:7:\"cs_code\";s:9:\"MGI-C0014\";s:6:\"i_code\";s:10:\"MP-MF-0006\";s:4:\"name\";s:13:\"Metalize Film\";s:6:\"s_code\";s:9:\"WMA-S0003\";s:3:\"qty\";s:2:\"55\";s:6:\"status\";i:0;s:5:\"stock\";s:3:\"756\";}}', '2019-09-04', 0, 1, 1, '2019-07-07 19:29:33');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(10) NOT NULL,
  `name` varchar(10) NOT NULL,
  `display_name` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `name`, `display_name`, `created_at`) VALUES
(1, 'admin', 'Admin Inv', '2018-06-20 20:37:17'),
(2, 'operator', 'Operator', '2019-06-26 19:24:22');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `setting_id` int(10) NOT NULL,
  `send_mail` varchar(50) NOT NULL,
  `send_pass` varchar(30) NOT NULL,
  `protocol` varchar(10) NOT NULL,
  `smtp_host` varchar(50) NOT NULL,
  `smtp_port` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`setting_id`, `send_mail`, `send_pass`, `protocol`, `smtp_host`, `smtp_port`) VALUES
(1, 'alternate.septian@gmail.com', 'januari1993', 'smtp', 'ssl://smtp.gmail.com', 465);

-- --------------------------------------------------------

--
-- Table structure for table `supliers`
--

CREATE TABLE `supliers` (
  `suplier_id` int(10) NOT NULL,
  `s_code` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supliers`
--

INSERT INTO `supliers` (`suplier_id`, `s_code`, `name`, `created_at`) VALUES
(3, 'WMA-S0003', 'Wira Mustika Abadi', '2019-07-07 03:51:43'),
(4, 'J-S0004', 'Januar', '2019-07-07 03:51:52'),
(5, 'MP-S0005', 'Modern Plastic', '2019-07-07 03:52:03');

-- --------------------------------------------------------

--
-- Table structure for table `token_password`
--

CREATE TABLE `token_password` (
  `token_id` int(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `token` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `type_id` int(10) NOT NULL,
  `t_code` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`type_id`, `t_code`, `name`, `created_at`) VALUES
(5, 'M/P', 'M P', '2019-07-07 03:52:56'),
(6, 'P/P', 'P P', '2019-07-07 03:53:00'),
(7, 'P/Q', 'P Q', '2019-07-07 03:53:13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role_id` int(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `address`, `phone`, `email`, `password`, `role_id`, `created_at`) VALUES
(1, 'Arman Septian (IT)', 'Jl. Kh Agus Salim Kp. Bulak Slamet No.8 RT06/RW08 Bekasi Jaya, Bekasi TImur 17112', '089517227009', 'admin@gmail.com', '5456db00ec97635f7d998e3290a01b6d', 1, '2019-01-07 20:31:33'),
(4, 'Arman Septian', 'Bekasi', '0895182323', 'idah.dpooh@gmail.com', '5456db00ec97635f7d998e3290a01b6d', 2, '2019-07-24 15:12:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `c_code` (`c_code`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `s_code` (`cs_code`);

--
-- Indexes for table `incoming`
--
ALTER TABLE `incoming`
  ADD PRIMARY KEY (`incoming_id`),
  ADD KEY `user_id_created` (`user_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `i_code` (`i_code`,`t_code`,`c_code`,`s_code`),
  ADD KEY `s_code` (`s_code`),
  ADD KEY `t_code` (`t_code`),
  ADD KEY `c_code` (`c_code`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `outgoing`
--
ALTER TABLE `outgoing`
  ADD PRIMARY KEY (`outgoing_id`),
  ADD KEY `user_id_created` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `supliers`
--
ALTER TABLE `supliers`
  ADD PRIMARY KEY (`suplier_id`),
  ADD UNIQUE KEY `s_code` (`s_code`);

--
-- Indexes for table `token_password`
--
ALTER TABLE `token_password`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`type_id`),
  ADD UNIQUE KEY `t_code` (`t_code`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `incoming`
--
ALTER TABLE `incoming`
  MODIFY `incoming_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `item_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `log_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `outgoing`
--
ALTER TABLE `outgoing`
  MODIFY `outgoing_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `setting_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `supliers`
--
ALTER TABLE `supliers`
  MODIFY `suplier_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `token_password`
--
ALTER TABLE `token_password`
  MODIFY `token_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `type_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `incoming`
--
ALTER TABLE `incoming`
  ADD CONSTRAINT `incoming_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_ibfk_1` FOREIGN KEY (`s_code`) REFERENCES `supliers` (`s_code`),
  ADD CONSTRAINT `items_ibfk_2` FOREIGN KEY (`t_code`) REFERENCES `types` (`t_code`),
  ADD CONSTRAINT `items_ibfk_3` FOREIGN KEY (`c_code`) REFERENCES `categories` (`c_code`);

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `outgoing`
--
ALTER TABLE `outgoing`
  ADD CONSTRAINT `outgoing_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `token_password`
--
ALTER TABLE `token_password`
  ADD CONSTRAINT `token_password_ibfk_1` FOREIGN KEY (`email`) REFERENCES `users` (`email`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
