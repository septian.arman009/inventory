 
function postData(url, data, callback) {
	$.ajax({
		type: "POST",
		contentType: "application/json",
		dataType: "json",
		url: url,
		data: JSON.stringify(data),
		success: function (response) {
			return callback(null, response);
		},
		error: function (err) {
			return callback(true, err);
		}
	});
}

function loadView(url, div) {
	$.ajax({
		url: url,
		beforeSend: function () {},
		success: function (data) {
			document.cookie = "admin_url="+url;
			$(div).html(data);
		}
	});
}

function do_something(url) {
	$.ajax({
		type: "POST",
		url: url
	});
}

function _(id) {
	return document.getElementById(id);
}

function logout() {
	document.cookie = "main_id=";
	document.cookie = "main_sub_id=";
	document.cookie = "sub_id=";
	document.cookie = "admin_url=";
	do_something("main_controller/logout");
	setTimeout(() => {
		location.reload();
	}, 500);
}

function ValidateEmail(mail, btn, id_msg) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
		$(id_msg).hide();
		return true;
	}
	$(id_msg).show();
	$(btn).attr("disabled", "disabled");
}

function isValidDate(date) {
	var temp = date.split('-');
	var d = new Date(temp[0] + '-' + temp[1] + '-' + temp[2]);
	return (d && (d.getMonth() + 1) == temp[0] && d.getDate() == Number(temp[1]) && d.getFullYear() == Number(temp[2]) &&
		Number(temp[2].length == 4) && Number(temp[2] > 1000));
}

function main_menu(main_id, admin_url) {
	var main_id_old = document.cookie.replace(/(?:(?:^|.*;\s*)main_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(main_id_old).removeClass("active");

	var main_sub_id = document.cookie.replace(/(?:(?:^|.*;\s*)main_sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(main_sub_id).removeClass("active");

	var sub_id = document.cookie.replace(/(?:(?:^|.*;\s*)sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(sub_id).removeClass("active_c");

	$(main_id).addClass("active");

	document.cookie = "main_id=" + main_id;
	document.cookie = "main_sub_id=";
	document.cookie = "sub_id=";
	document.cookie = "admin_url=" + admin_url;

	loadView(admin_url, '.content');
}

function sub_menu(main_sub_id, sub_id, admin_url) {
	var main_id = document.cookie.replace(/(?:(?:^|.*;\s*)main_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(main_id).removeClass("active");

	var main_sub_id_old = document.cookie.replace(/(?:(?:^|.*;\s*)main_sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(main_sub_id_old).removeClass("active");

	var sub_id_old = document.cookie.replace(/(?:(?:^|.*;\s*)sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(sub_id_old).removeClass("active_c");

	$(main_sub_id).addClass("active");
	$(sub_id).addClass("active_c");

	document.cookie = "main_id=";
	document.cookie = "main_sub_id=" + main_sub_id;
	document.cookie = "sub_id=" + sub_id;
	document.cookie = "admin_url=" + admin_url;

	loadView(admin_url, '.content');
}

function to_rp(fix) {
	var number_string = fix.toString(),
		sisa = number_string.length % 3,
		rupiah = number_string.substr(0, sisa),
		ribuan = number_string.substr(sisa).match(/\d{3}/g);
	if (ribuan) {
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}
	return 'Rp. ' + rupiah;
}

String.prototype.replaceAll = function (str1, str2, ignore) {
	return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ? "gi" : "g")), (typeof (str2) == "string") ? str2.replace(/\$/g, "$$$$") : str2);
}