function signin() {
	var data = {
		email: $("#email").val(),
		password: $("#password").val()
	};
	postData("auth_controller/signinProccess", data, function (err, response) {
		if (response) {
			if (response.status == "success") {
				window.location = "main";
			} else {
				swal("Terjadi Kesalahan", "Signin gagal, cek username dan password anda.", "error");
			}
		}
	});
}

function check() {
	var email = $("#email").val();
	if (email != '') {
		if (ValidateEmail(email, "#send_mail", "#invalid_email")) {
			check_email();
		}
	}else{
		$("#send_token").attr('disabled', 'disabled');
	}
}

function check_email() {
	var data = {
		email: $("#email").val()
	}
	postData('auth_controller/check_email', data, function (err, response) {
		if (response) {
			var status = response.status;
			if (status == 'success') {
				$("#notexist").hide();
				check_token(email);
			} else {
				$("#notexist").show();
				$("#send_token").attr('disabled', 'disabled');
			}
		} else {
			console.log('ini error : ', err);
		}
	});
}

function check_token(email) {
	var data = {
		email: $("#email").val()
	}
	postData('auth_controller/check_token', data, function (err, response) {
		if (response) {
			var status = response.status;
			if (status == 'success') {
				$("#token-exist").hide();
				$('#send_token').removeAttr('disabled');
			} else {
				$("#token-exist").show();
				$("#send_token").attr('disabled', 'disabled');
			}
		} else {
			console.log('ini error : ', err);
		}
	});
}

function check_password() {
	var password = $("#password").val();
	var confirm = $("#confirm").val();
	var passlen = password.length;
	var conlen = confirm.length;

	if (passlen != '') {
		if (passlen < 8) {
			$("#passless").show();
		} else {
			$("#passless").hide();
		}
	}else{
		$("#passless").hide();
	}

	if (confirm != '') {
		if (conlen < 8) {
			$("#conless").show();
		} else {
			$("#conless").hide();
		}
	}else{
		$("#conless").hide();
	}

	if (password != '' && confirm != '') {
		if (password == confirm) {
			$("#notmatch").hide();
			if (password.length >= 8 && confirm.length >= 8) {
				$("#reset_password").removeAttr('disabled');
			} else {
				$("#reset_password").attr('disabled', 'disabled');
			}
		} else {
			$("#notmatch").show();
			$("#reset_password").attr('disabled', 'disabled');
		}
	} else {
		$("#reset_password").attr('disabled', 'disabled');
	}
}

function send_token() {
	$("#send_token").hide();
	$("#loading").show();
	var data = {
		email: $("#email").val()
	};
	postData("auth_controller/send_token", data, function (err, response) {
		if (response) {
			if (response.status == "success") {
				swal("Sukses", "Berhasil mengirim link reset password.", "success");
				$("#send_token").show();
				$("#loading").hide();
				$("#email").val('');
				setTimeout(() => {
					check();
				}, 100);
			} else {
				swal("Terjadi Kesalahan", "Gagal mengirim token reset password.", "error");
				$("#send_token").show();
				$("#loading").hide();
			}
		}
	});
}
