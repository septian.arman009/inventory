<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        $this->load->model('d_table');
        auth();
    }

    public function form($form, $id, $param)
    {
        $data['id'] = $id;
        if ($form == 'users') {
            $data['roles'] = $this->main_model->gda1p('roles');
            $this->load->view('content/admin/master/user/form', $data);
        }else if ($form == 'supliers') {
            $data['s_code'] = generate_code_m('S', 'supliers');
            $this->load->view('content/admin/master/suplier/form', $data);
        }else if ($form == 'types') {
            $this->load->view('content/admin/master/type/form', $data);
        }else if ($form == 'categories') {
            $data['c_code'] = generate_code('CAT', 'categories');
            $this->load->view('content/admin/master/category/form', $data);
        }else if ($form == 'items') {
            $data['i_code'] = generate_code('', 'items');
            $data['t_code'] = $this->main_model->gda1p('types');
            $data['c_code'] = $this->main_model->gda1p('categories');
            $data['s_code'] = $this->main_model->gda1p('supliers');
            $this->load->view('content/admin/master/item/form', $data);
        }else if ($form == 'customers') {
            $data['cs_code'] = generate_code_m('C', 'customers');
            $this->load->view('content/admin/master/customer/form', $data);
        }
        
    }

    public function user()
    {
        if(role(['admin'],false)){
            $this->load->view('content/admin/master/user/index');
        }else{
            $data['to_show'] = '#users_normal';
            $data['to_hide'] = '#users_spin';
            $this->load->view('content/error_404', $data);
        }
    }

    public function user_table()
    {
        $id = 'user_id';
        $table = 'users';
        $column = array(
            'user_id',
            'name',
            'email',
            'address',
            'phone'
        );
        show_table($id, $table, $column, 'null');
    }

    public function customer()
    {
        if(role(['admin'],false)){
            $this->load->view('content/admin/master/customer/index');
        }else{
            $data['to_show'] = '#customers_normal';
            $data['to_hide'] = '#customers_spin';
            $this->load->view('content/error_404', $data);
        }
    }

    public function customer_table()
    {
        $id = 'customer_id';
        $table = 'customers';
        $column = array(
            'customer_id',
            'cs_code',
            'name'
        );
        show_table($id, $table, $column, 'null');
    }

    public function suplier()
    {
        if(role(['admin'],false)){
            $this->load->view('content/admin/master/suplier/index');
        }else{
            $data['to_show'] = '#supliers_normal';
            $data['to_hide'] = '#supliers_spin';
            $this->load->view('content/error_404', $data);
        }
    }

    public function suplier_table()
    {
        $id = 'suplier_id';
        $table = 'supliers';
        $column = array(
            'suplier_id',
            's_code',
            'name'
        );
        show_table($id, $table, $column, 'null');
    }

    public function type()
    {
        if(role(['admin'],false)){
            $this->load->view('content/admin/master/type/index');
        }else{
            $data['to_show'] = '#types_normal';
            $data['to_hide'] = '#types_spin';
            $this->load->view('content/error_404', $data);
        }
    }

    public function type_table()
    {
        $id = 'type_id';
        $table = 'types';
        $column = array(
            'type_id',
            't_code',
            'name'
        );
        show_table($id, $table, $column, 'null');
    }

    public function category()
    {
        if(role(['admin'],false)){
            $this->load->view('content/admin/master/category/index');
        }else{
            $data['to_show'] = '#categories_normal';
            $data['to_hide'] = '#categories_spin';
            $this->load->view('content/error_404', $data);
        }
    }

    public function category_table()
    {
        $id = 'category_id';
        $table = 'categories';
        $column = array(
            'category_id',
            'c_code',
            'name'
        );
        show_table($id, $table, $column, 'null');
    }

    public function item()
    {
        if(role(['admin'],false)){
            $this->load->view('content/admin/master/item/index');
        }else{
            $data['to_show'] = '#items_normal';
            $data['to_hide'] = '#items_spin';
            $this->load->view('content/error_404', $data);
        }
    }

    public function item_table()
    {
        $id = 'item_id';
        $table = 'items';
        $column = array(
            'item_id',
            'i_code',
            'c_code',
            'name',
            'qty'
        );
        show_table($id, $table, $column, 'null');
    }

    public function user_action()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;
        $data['name'] = ucwords($obj->name);
        $data['email'] = $obj->email;
        $data['address'] = ucwords($obj->address);
        $data['phone'] = $obj->phone;
        $data['role_id'] = $obj->role_id;

        if ($id == 'null') {
            $data['password'] = md5($obj->password);
            $log_message = "<b>Registrasi Pengguna Baru</b> : Mendaftarkan pengguna baru dengan email : {$data['email']} 
            cek email untuk melihat password yang sudah dikirimkan";
            do_action($id, $table, 'user_id', $data, $log_message);
        } else {
            $log_message = "<b>Memperbarui Pengguna</b> : Memperbarui pengguna dengan email : {$data['email']}";
            do_action($id, $table, 'user_id', $data, $log_message);
        }
    }

    public function type_action()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;
        $data['t_code'] = strtoupper($obj->t_code);
        $data['name'] = ucwords($obj->name);

        if ($id == 'null') {
            $log_message = "<b>Tambah Jenis Barang</b> : Mendaftarkan jenis barang baru dengan kode : {$data['t_code']}";
            do_action($id, $table, 'type_id', $data, $log_message);
        } else {
            $log_message = "<b>Memperbarui Jenis Barang</b> : Memperbarui jenis barang baru dengan kode : {$data['t_code']}";
            do_action($id, $table, 'type_id', $data, $log_message);
        }
    }

    public function category_action()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;
        $data['c_code'] = $obj->c_code;
        $data['name'] = ucwords($obj->name);

        if ($id == 'null') {
            $log_message = "<b>Tambah Kategori Barang</b> : Mendaftarkan kategori barang baru dengan kode : {$data['c_code']}";
            do_action($id, $table, 'category_id', $data, $log_message);
        } else {
            $log_message = "<b>Memperbarui Kategori Barang</b> : Memperbarui kategori barang baru dengan kode : {$data['c_code']}";
            do_action($id, $table, 'category_id', $data, $log_message);
        }
    }

    public function customer_action()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;
        $data['cs_code'] = $obj->cs_code;
        $data['name'] = ucwords($obj->name);

        if ($id == 'null') {
            $log_message = "<b>Tambah Customer</b> : Mendaftarkan customer baru dengan kode : {$data['cs_code']}";
            do_action($id, $table, 'customer_id', $data, $log_message);
        } else {
            $log_message = "<b>Memperbarui Customer </b> : Memperbarui customer baru dengan kode : {$data['cs_code']}";
            do_action($id, $table, 'customer_id', $data, $log_message);
        }
    }


    public function suplier_action()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;
        $data['s_code'] = $obj->s_code;
        $data['name'] = ucwords($obj->name);

        if ($id == 'null') {
            $log_message = "<b>Tambah Suplier</b> : Mendaftarkan suplier baru dengan kode : {$data['s_code']}";
            do_action($id, $table, 'suplier_id', $data, $log_message);
        } else {
            $log_message = "<b>Memperbarui Suplier </b> : Memperbarui suplier baru dengan kode : {$data['s_code']}";
            do_action($id, $table, 'suplier_id', $data, $log_message);
        }
    }

    public function item_action()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;
        $data['i_code'] = $obj->i_code;
        $data['name'] = ucwords($obj->name);
        $data['t_code'] = $obj->t_code;
        $data['c_code'] = $obj->c_code;
        $data['s_code'] = $obj->s_code;
        $data['qty'] = $obj->qty;

        if ($id == 'null') {
            $log_message = "<b>Tambah Barang</b> : Mendaftarkan barang baru dengan kode : {$data['i_code']}";
            do_action($id, $table, 'item_id', $data, $log_message);
        } else {
            $log_message = "<b>Memperbarui Barang</b> : Memperbarui barang baru dengan kode : {$data['i_code']}";
            do_action($id, $table, 'item_id', $data, $log_message);
        }
    }

}
