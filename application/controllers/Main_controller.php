<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        auth();
    }

    public function index()
    {
        $data['date_report'] = explode('-', date('Y-m-d'));
        $this->load->view('content/admin/main', $data);
    }

    public function home($page)
    {
        $perPage = 10;
        $total_row = $this->main_model->count('logs', 'log_id');
        $data['all'] = ceil($total_row / 10);
        if ($page != 1) {
            $end = $perPage * $page;
            $start = $end - 10;
            $data['page'] = $page;
            if ($start > 0) {
                $data['logs'] = $this->main_model->limit($start, $end, 'logs', 'log_id');
                $this->load->view('content/admin//home/home', $data);
            } else {
                $data['logs'] = '';
                $this->load->view('content/admin/home/home', $data);
            }
        } else {
            $data['page'] = $page;
            $data['logs'] = $this->main_model->limit(0, $perPage, 'logs', 'log_id');
            $this->load->view('content/admin//home/home', $data);
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('inv_in');
    }

    public function destroy($table, $column)
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        if ($table == 'users' && $id == whoIAM()['id']) {
            to_dump($id);
        } else if (role(['admin'], true)) {
            if($table == 'users'){
                $this->main_model->destroy('incoming', 'user_id', $id);
                $this->main_model->destroy('incoming', 'user_updated', $id);
                $this->main_model->destroy('outgoing', 'user_id', $id);
                $this->main_model->destroy('outgoing', 'user_updated', $id);
                $this->main_model->destroy('logs', 'user_id', $id);
                $message = "<b>Menghapus Pengguna</b> : Menghapus pengguna dengan Nama : {$this->main_model->gdo4p($table, 'name', $column, $id)}";
                $this->do_destroy($table, $column, $id, $message);
            }else if($table == 'types'){
                $exist = $this->main_model->exixt5p('items', 't_code', 'types', 'type_id', $id);
                if(!$exist){
                    $message = "<b>Menghapus Jenis Barang</b> : Menghapus jenis barang dengan Nama : {$this->main_model->gdo4p($table, 'name', $column, $id)}";
                    $this->do_destroy($table, $column, $id, $message);
                }else{
                    r_error();
                }
            }else if($table == 'categories'){
                $exist = $this->main_model->exixt5p('items', 'c_code', 'categories', 'category_id', $id);
                if(!$exist){
                    $message = "<b>Menghapus Kategori Barang</b> : Menghapus kategori barang dengan Nama : {$this->main_model->gdo4p($table, 'name', $column, $id)}";
                    $this->do_destroy($table, $column, $id, $message);
                }else{
                    r_error();
                }
            }else if($table == 'items'){
                $i_code = $this->main_model->gdo4p('items', 'i_code', 'item_id', $id);
                $check = $this->main_model->gda3pl('incoming', 'incoming', $i_code, 'incoming_id', 'asc');
                $check1 = $this->main_model->gda3pl('outgoing', 'outgoing', $i_code, 'outgoing_id', 'asc');

                if(!$check && !$check1){
                    $message = "<b>Menghapus Barang</b> : Menghapus barang dengan Nama : {$this->main_model->gdo4p($table, 'name', $column, $id)}";
                    $this->do_destroy($table, $column, $id, $message);
                }else{
                    r_error();
                }
            }else if($table == 'supliers'){
                $exist = $this->main_model->exixt5p('items', 's_code', 'supliers', 'suplier_id', $id);
                if(!$exist){
                    $message = "<b>Menghapus Suplier</b> : Menghapus suplier dengan Nama : {$this->main_model->gdo4p($table, 'name', $column, $id)}";
                    $this->do_destroy($table, $column, $id, $message);
                }else{
                    r_error();
                }
            }else if($table == 'customers'){
                $message = "<b>Menghapus Suplier</b> : Menghapus customer dengan Nama : {$this->main_model->gdo4p($table, 'name', $column, $id)}";
                $this->do_destroy($table, $column, $id, $message);
            }else if($table == 'incoming'){
                $incoming = unserialize($this->main_model->gdo4p('incoming', 'incoming', 'incoming_id', $id));
                foreach ($incoming as $key => $value) {
                    $this->main_model->update_add1(-$value['qty'], 'items', 'qty', 'i_code', $value['i_code']);
                }
                $message = "<b>Menghapus Barang Masuk</b> : Menghapus barang masuk dengan Kode Transaksi : {$this->main_model->gdo4p($table, 'inc_code', $column, $id)}";
                $this->do_destroy($table, $column, $id, $message);
            }else if($table == 'outgoing'){
                $outgoing = unserialize($this->main_model->gdo4p('outgoing', 'outgoing', 'outgoing_id', $id));
                foreach ($outgoing as $key => $value) {
                    $this->main_model->update_add1($value['qty'], 'items', 'qty', 'i_code', $value['i_code']);
                }
                $message = "<b>Menghapus Barang Keluar</b> : Menghapus barang keluar dengan Kode Transaksi : {$this->main_model->gdo4p($table, 'otg_code', $column, $id)}";
                $this->do_destroy($table, $column, $id, $message);
            }
        }
    }

    function do_destroy($table, $column, $id, $message){
        $this->main_model->destroy($table, $column, $id);
        logs($message);
        r_success();
    }

    public function single_data_check()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $value = $obj->value;
        $column = $obj->column;
        $column_id = $obj->column_id;
        $table = $obj->table;
        if ($id == 'null') {
            $check = $this->main_model->gda3p($table, $column, $value);
            if (!$check) {
                r_success();
            }
        } else {
            $old = $this->main_model->gdo4p($table, $column, $column_id, $id);
            if ($value == $old) {
                r_success();
            } else {
                $check = $this->main_model->gda3p($table, $column, $value);
                if (!$check) {
                    r_success();
                }
            }
        }
    }

    public function triple_data_check()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $value = $obj->value;
        $column = $obj->column;
        $column_id = $obj->column_id;
        $table = $obj->table;
        $table1 = $obj->table1;
        $table2 = $obj->table2;
        if ($id == 'null') {
            $check = $this->main_model->gda3p($table, $column, $value);
            $check1 = $this->main_model->gda3p($table1, $column, $value);
            $check2 = $this->main_model->gda3p($table2, $column, $value);
            if (!$check && !$check1 && !$check2) {
                r_success();
            }
        } else {
            $old = $this->main_model->gdo4p($table, $column, $column_id, $id);
            if ($value == $old) {
                r_success();
            } else {
                $check = $this->main_model->gda3p($table, $column, $value);
                $check1 = $this->main_model->gda3p($table1, $column, $value);
                $check2 = $this->main_model->gda3p($table2, $column, $value);
                if (!$check && !$check1 && !$check2) {
                    r_success();
                }
            }
        }
    }

    public function get_data()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $column = $obj->column;
        $table = $obj->table;
        $data = $this->main_model->gda3p($table, $column, $id);
        if ($data) {
            r_success_data($data);
        }
    }

    public function email_sender()
    {
        if (role(['admin'], false)) {
            $data['setting'] = $this->main_model->gda3p('setting', 'setting_id', 1);
            $this->load->view('content/admin/settings/email_sender', $data);
        } else {
            $data['to_show'] = '#email_sender_normal';
            $data['to_hide'] = '#email_sender_spin';
            $this->load->view('content/error_404', $data);
        }
    }

    public function update_email_sender()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['send_mail'] = $obj->email;
        $data['send_pass'] = $obj->password;
        $data['protocol'] = $obj->protocol;
        $data['smtp_host'] = $obj->smtp_host;
        $data['smtp_port'] = $obj->smtp_port;
        $update = $this->main_model->update('setting', $data, 'id', 1);
        if ($update) {
            $message = "<b>Konfigurasi Email Pengirim Pesan</b> : Memperbarui konfigurasi email pengiriman pesan dengan email : {$data['send_mail']}";
            logs($message);
            r_success();
        }
    }

    public function change_password()
    {
        $this->load->view('content/admin/settings/change_password');
    }

    public function check_old_password()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $password = md5($obj->old_password);
        $check = $this->main_model->gda5p('users', 'password', $password, 'user_id', $_SESSION['inv_in']['id']);
        if ($check) {
            r_success();
        }
    }

    public function update_password()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['password'] = md5($obj->password);
        $update = $this->main_model->update('users', $data, 'user_id', $_SESSION['inv_in']['id']);
        if ($update) {
            $message = "<b>Ganti Password</b> : Memperbarui password";
            logs($message);
            r_success();
        }
    }

}
