<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        $this->load->model('d_table');
        auth();
    }

    public function tncReport($start, $end, $year)
    {
        if(role(['admin'],false)){
            $data['month'] = indoMonthIndex();
            $data['year'] = $year;
            $data['indoMonth'] = indoMonth();
            $data['start'] = $start;
            $data['end'] = $end;
            $date1 = explode('-', $start);
            $date2 = explode('-', $end);
            $this->load->view('content/admin/report/tncReport/index', $data);
        }else{
            $data['to_show'] = '#incReport_normal';
            $data['to_hide'] = '#incReport_spin';
            $this->load->view('content/error_404', $data);
        }
    }

    public function tncDetail($start, $end, $year)
    {
        if(role(['admin', 'operator'],false)){
            if (!isset($_COOKIE['tab_active'])) {
                $data['tab_active'] = '#tab-first';
            } else {
                $data['tab_active'] = $_COOKIE['tab_active'];
            }
            $data['incoming'] = $this->main_model->between('incoming', 'date(inc_date)', $start, $end);
            $data['outgoing'] = $this->main_model->between('outgoing', 'date(otg_date)', $start, $end);
            $data['month'] = to_date($start).' sampai '.to_date($end);
            $data['year'] = $year;
            $this->load->view('content/admin/report/tncReport/tncDetail', $data);
        }else{
            $this->load->view('content/error_404');
        }
    }

    public function tncGraph($year)
    {

        $data = array(
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
            8 => 0,
            9 => 0,
            10 => 0,
            11 => 0,
            12 => 0
        );

        $data1 = array(
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
            8 => 0,
            9 => 0,
            10 => 0,
            11 => 0,
            12 => 0
        );

        $incoming = $this->main_model->gda3p('incoming', 'year(inc_date)', $year);
        foreach ($incoming as $key => $value) {
            $date = explode('-', $value['inc_date']);
            for($i = 1; $i <= 12; $i++){
                if($date[1] == $i){
                    $data[$i] += array_sum(array_column(unserialize($value['incoming']), 'qty'));
                }
            }
        }

        $outgoing = $this->main_model->gda3p('outgoing', 'year(otg_date)', $year);
        foreach ($outgoing as $key => $value) {
            $date = explode('-', $value['otg_date']);
            for($i = 1; $i <= 12; $i++){
                if($date[1] == $i){
                    $data1[$i] += array_sum(array_column(unserialize($value['outgoing']), 'qty'));
                }
            }
        }

        foreach ($data as $key => $value) {
            $row['data'][] = $value;
        }

        foreach ($data1 as $key => $value) {
            $row1['data'][] = $value;
        }

        $row['name'] = 'Barang Masuk';
        $row1['name'] = 'Barang Keluar';
        $result = array();
        array_push($result, $row);
        array_push($result, $row1);
        print json_encode($result, JSON_NUMERIC_CHECK);
    }

    public function printIncReport($id){
        $incData = $this->main_model->gda3p('incoming', 'incoming_id', $id);
        $data['incData'] = $incData;
        $data['date'] = explode('-', $incData[0]['inc_date']);
        $data['incList'] = unserialize($incData[0]['incoming']);

        $this->load->library('fpdf');
        $this->load->view('content/admin/report/tncReport/incPrint', $data);
    }

    public function printOtgReport($id){
        $otgData = $this->main_model->gda3p('outgoing', 'outgoing_id', $id);
        $data['otgData'] = $otgData;
        $data['date'] = explode('-', $otgData[0]['otg_date']);
        $data['otgList'] = unserialize($otgData[0]['outgoing']);

        $this->load->library('fpdf');
        $this->load->view('content/admin/report/tncReport/otgPrint', $data);
    }

    public function printAllIncReport($month, $year){
        
        $normal_month = str_replace('%20', ' ', $month);
        $normal_explode = explode(' sampai ', $normal_month);
        $start = normal_date($normal_explode[0]);
        $end = normal_date($normal_explode[1]);

        $data['month'] = $normal_month;
        $data['incData'] = $this->main_model->between('incoming', 'date(inc_date)', $start, $end);
        
        $this->load->library('fpdf');
        $this->load->view('content/admin/report/tncReport/incAllPrint', $data);
    }

    public function printAllOtgReport($month, $year){
        $normal_month = str_replace('%20', ' ', $month);
        $normal_explode = explode(' sampai ', $normal_month);
        $start = normal_date($normal_explode[0]);
        $end = normal_date($normal_explode[1]);

        $data['month'] = $normal_month;
        $data['otgData'] = $this->main_model->between('outgoing', 'date(otg_date)', $start, $end);

        $this->load->library('fpdf');
        $this->load->view('content/admin/report/tncReport/otgAllPrint', $data);
    }

    public function masterReport(){
        $this->load->view('content/admin/report/masterReport/index');
    }

    public function printItems(){
        $data['items'] = $this->main_model->gda1p('items');
        $this->load->library('fpdf');
        $this->load->view('content/admin/report/masterReport/items', $data);
    }

    public function printUsers(){
        $data['users'] = $this->main_model->gda1p('users');
        $this->load->library('fpdf');
        $this->load->view('content/admin/report/masterReport/users', $data);
    }

    public function printCustomers(){
        $data['customers'] = $this->main_model->gda1p('customers');
        $this->load->library('fpdf');
        $this->load->view('content/admin/report/masterReport/customers', $data);
    }

    public function printSupliers(){
        $data['supliers'] = $this->main_model->gda1p('supliers');
        $this->load->library('fpdf');
        $this->load->view('content/admin/report/masterReport/supliers', $data);
    }
}
