<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        $this->load->model('d_table');
        auth();
    }

    public function incoming($id)
    {
        if(role(['admin', 'operator'],false)){
            $data['id'] = $id;
            if($id == 'null'){
                $status = $this->main_model->limitWhere(0, 1,'incoming', 'status', 1, 'incoming_id');
                if($status){
                    $data['inc_code'] = $status[0]['inc_code'];
                    $data['date'] = to_date_bootstrap($status[0]['inc_date']);
                    $data['status'] = 1;
                    $data['incoming_list'] = unserialize($status[0]['incoming']);
                }else{
                    $data['inc_code'] = generate_code_tnc('incoming');
                    $data['date'] = date('d-m-Y');
                    $data['status'] = 0;
                    $data['incoming_list'] = array();
                }
            }else{
                $incoming = $this->main_model->gda3p('incoming', 'incoming_id', $id);
                $data['inc_code'] = $incoming[0]['inc_code'];
                $data['date'] = to_date_bootstrap($incoming[0]['inc_date']);
                $data['status'] = 1;    
                $data['incoming_list'] = unserialize($incoming[0]['incoming']);
            }
            $this->load->view('content/admin/transaction/incoming/index', $data);
        }else{
            $data['to_show'] = '#incoming_normal';
            $data['to_hide'] = '#incoming_spin';
            $this->load->view('content/error_404', $data);
        }
    }

    public function search($table)
    {
        $name = $this->input->get('query');
        $data = $this->db->query("select name from {$table} where name like '%$name%'")->result();
        echo json_encode($data);
    }

    public function addItem($id)
    {   
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $tnc_code = $obj->tnc_code;
        $tnc_table = $obj->tnc_table;
        $tnc_column = $obj->tnc_column;
        $i_code = $obj->i_code;
        $qty = $obj->qty;
        $tnc_date = to_date_mysql($obj->tnc_date);

        $check = $this->main_model->gda3p($tnc_table, $tnc_column, $tnc_code);
        $item = $this->main_model->gda3p('items', 'i_code', $i_code);
        if(!$check){
            if($tnc_table == 'incoming'){
                $this->addNewItem($item, $qty, $tnc_code, $tnc_date, $tnc_table, '');
            }else{
                $cs_code = $obj->cs_code;
                $this->addNewItem($item, $qty, $tnc_code, $tnc_date, $tnc_table, $cs_code);
            }
        }else{
            $tnc_data = unserialize($check[0][$tnc_table]);
            if($id == 'null'){
                if($tnc_table == 'incoming'){
                    $check_array = arraySearch($tnc_data, 'i_code', $i_code);
                    $this->addExistItemINC($item, $qty, $check_array, $tnc_data, $tnc_code, 0);
                }else{
                    $cs_code = $obj->cs_code;
                    $check_array = arraySearch2($tnc_data, 'cs_code', $cs_code, 'i_code', $i_code);
                    $this->addExistItemOTG($item, $qty, $check_array, $tnc_data, $tnc_code, 0, $cs_code);
                }
            }else{
                if($tnc_table == 'incoming'){
                    $check_array = arraySearch($tnc_data, 'i_code', $i_code);
                    if($check_array === false){
                        $this->addExistItemINC($item, $qty, $check_array, $tnc_data, $tnc_code, 1);
                    }else{
                        $items_status = $tnc_data[$check_array]['status'];
                        if($items_status == 1){
                            $this->addExistItemINC($item, $qty, $check_array, $tnc_data, $tnc_code, 1);
                        }else{
                            r_error();
                        }
                    }
                }else{
                    $cs_code = $obj->cs_code;
                    $check_array = arraySearch2($tnc_data, 'cs_code', $cs_code, 'i_code', $i_code);
                    if($check_array === false){
                        $this->addExistItemOTG($item, $qty, $check_array, $tnc_data, $tnc_code, 1, $cs_code);
                    }else{
                        $items_status = $tnc_data[$check_array]['status'];
                        if($items_status == 1){
                            $this->addExistItemOTG($item, $qty, $check_array, $tnc_data, $tnc_code, 1, $cs_code);
                        }else{
                            r_error();
                        }
                    }
                }
            }
        }
    }

    public function addNewItem($item, $qty, $tnc_code, $tnc_date, $tnc_table, $cs_code)
    {
        if($tnc_table == 'incoming'){
            $items[] =  array(
                'i_code' => $item[0]['i_code'],
                'name' => $item[0]['name'],
                's_code' => $item[0]['s_code'],
                'qty' => $qty,
                'status' => 0
            );
            $data['inc_code'] = $tnc_code;
            $data['incoming'] = serialize($items);
            $data['inc_date'] = $tnc_date;
        }else{
            $items[] =  array(
                'cs_code' => $cs_code,
                'i_code' => $item[0]['i_code'],
                'name' => $item[0]['name'],
                's_code' => $item[0]['s_code'],
                'qty' => $qty,
                'status' => 0
            );
            $data['otg_code'] = $tnc_code;
            $data['outgoing'] = serialize($items);
            $data['otg_date'] = $tnc_date;
        }
     
        $data['status'] = 1;
        $data['user_id'] = whoIAM()['id'];

        $save = $this->main_model->store($tnc_table, $data);
        if($save){
            r_success();
        }
    }

    public function addExistItemINC($item, $qty, $check_array, $tnc_data, $tnc_code, $status)
    {   
        $items =  array(
            'i_code' => $item[0]['i_code'],
            'name' => $item[0]['name'],
            's_code' => $item[0]['s_code'],
            'qty' => $qty,
            'status' => $status
        );
        
        if ($check_array === false) {
            array_push($tnc_data, $items);
        } else {
            $tnc_data[$check_array] = $items;
        }

        $data['incoming'] = serialize($tnc_data);
        $data['user_updated'] = whoIAM()['id'];
        $update = $this->main_model->update('incoming', $data, 'inc_code', $tnc_code);
        if ($update) {
            r_success();
        }
    }

    public function addExistItemOTG($item, $qty, $check_array, $tnc_data, $tnc_code, $status, $cs_code)
    {   
        $items =  array(
            'cs_code' => $cs_code,
            'i_code' => $item[0]['i_code'],
            'name' => $item[0]['name'],
            's_code' => $item[0]['s_code'],
            'qty' => $qty,
            'status' => $status
        );
        
        if ($check_array === false) {
            array_push($tnc_data, $items);
        } else {
            $tnc_data[$check_array] = $items;
        }

        $data['outgoing'] = serialize($tnc_data);
        $data['user_updated'] = whoIAM()['id'];
        $update = $this->main_model->update('outgoing', $data, 'otg_code', $tnc_code);
        if ($update) {
            r_success();
        }
    }

    public function removeItem($id)
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $tnc_table = $obj->tnc_table;
        $tnc_column = $obj->tnc_column;
        $tnc_code = $obj->tnc_code;
        $i_code = $obj->i_code;

        $tnc_data = unserialize($this->main_model->gdo4p($tnc_table, $tnc_table, $tnc_column, $tnc_code));
        if (count($tnc_data) > 1) {

            if($tnc_table == 'incoming'){
                $index = arraySearch($tnc_data, 'i_code', $i_code);
            }else{
                $cs_code = $obj->cs_code;
                $index = arraySearch2($tnc_data, 'cs_code', $cs_code, 'i_code', $i_code);
            }

            if($id == 'null'){
                unset($tnc_data[$index]);
            }else{
                if($tnc_data[$index]['status'] == 0){
                    if($tnc_table == 'incoming'){
                        $update_items = $this->main_model->updateSingleColumn('items', 'qty', "qty-{$tnc_data[$index]['qty']}", 'i_code', $i_code);  
                    }else{
                        $update_items = $this->main_model->updateSingleColumn('items', 'qty', "qty+{$tnc_data[$index]['qty']}", 'i_code', $i_code);  
                    }
                    if($update_items){
                        unset($tnc_data[$index]);
                    }
                }else{
                    unset($tnc_data[$index]);
                    
                }
            }

            $data[$tnc_table] = serialize($tnc_data);
            $data['user_updated'] = whoIAM()['id'];
            $update = $this->main_model->update($tnc_table, $data, $tnc_column, $tnc_code);
            if ($update) {
                r_success();
            }
        }
    }

    public function saveTnc($id)
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $tnc_table = $obj->tnc_table;
        $tnc_column = $obj->tnc_column;
        $tnc_code = $obj->tnc_code;
        
        $tnc_data = unserialize($this->main_model->gdo4p($tnc_table, $tnc_table, $tnc_column, $tnc_code));
        $data_items = array();

        if($tnc_table == 'incoming'){
            $items = $this->main_model->whereIn('items', 'i_code', takeArrayValue($tnc_data, 'i_code'));
        }else{
            $cs_code = takeArrayValue($tnc_data, 'cs_code');
            if($id == 'null'){
                $arrayIcode = takeArrayValue($tnc_data, 'i_code');
                $qtyPerIcode = takeArrayValue1($tnc_data, 'i_code', 'qty');
                foreach ($arrayIcode as $key => $value) {
                    $stock[$value] = $this->main_model->gdo4p('items', 'qty', 'i_code', $value);
                    $data_items[] = array(
                        'i_code' => $value,
                        'qty' => $stock[$value] - $qtyPerIcode[$value]
                    );
                }
            }else{
                $statusEQ1 = takeArrayValueWhere($tnc_data, 'i_code', 'qty', 'status', 1);
                $arrayIcode = takeArrayValue($statusEQ1, 'i_code');
                $qtyPerIcode = takeArrayValue1($statusEQ1, 'i_code', 'qty');
                foreach ($arrayIcode as $key => $value) {
                    $stock[$value] = $this->main_model->gdo4p('items', 'qty', 'i_code', $value);
                    $data_items[] = array(
                        'i_code' => $value,
                        'qty' => $stock[$value] - $qtyPerIcode[$value]
                    );
                }
            }
        }

        $index = 0;
        foreach ($tnc_data as $key => $value) {
            if($id == 'null'){
                if($tnc_table == 'incoming'){
                    $stock = $items[arraySearch($items, 'i_code', $value['i_code'])]['qty'];
                    $tnc_data[arraySearch($tnc_data, 'i_code', $value['i_code'])]['stock']= ($stock + $value['qty']);
                    $data_items[] = array(
                        'i_code' => $value['i_code'],
                        'qty' => ($stock + $value['qty'])
                    );
                }else{
                    $tnc_data[arraySearch2($tnc_data, 'cs_code', $cs_code[$index], 'i_code', $value['i_code'])]['stock'] = $stock[$value['i_code']];
                    $stock[$value['i_code']] = ($stock[$value['i_code']] - $value['qty']);
                    if($stock[$value['i_code']] < 0){
                        r_error();
                        die();
                    }
                } 
            }else{
                if($value['status'] == 1){
                    if($tnc_table == 'incoming'){
                        $stock = $items[arraySearch($items, 'i_code', $value['i_code'])]['qty'];
                        $tnc_data[arraySearch($tnc_data, 'i_code', $value['i_code'])]['stock']= ($stock + $value['qty']);
                        $tnc_data[arraySearch($tnc_data, 'i_code', $value['i_code'])]['status']= 0;
                        $data_items[] = array(
                            'i_code' => $value['i_code'],
                            'qty' => ($stock + $value['qty'])
                        );
                    }else{
                        $tnc_data[arraySearch2($tnc_data, 'cs_code', $cs_code[$index], 'i_code', $value['i_code'])]['stock']= $stock[$value['i_code']];
                        $stock[$value['i_code']] = ($stock[$value['i_code']] - $value['qty']);
                        $tnc_data[arraySearch2($tnc_data, 'cs_code', $cs_code[$index], 'i_code', $value['i_code'])]['status']= 0;
                        if($stock[$value['i_code']] < 0){
                            r_error();
                            die();
                        }
                    }
                }
            }
            $index++;
        }

        if($data_items){
            $update_item = $this->main_model->updateMultiple('items', $data_items, 'i_code');
            if($update_item){
                $data[$tnc_table] = serialize($tnc_data);
                $data['status'] = 0;
                $data['user_updated'] = whoIAM()['id'];
                $update = $this->main_model->update($tnc_table, $data, $tnc_column, $tnc_code);
                if($update){
                    if($tnc_table == 'incoming'){
                        if($id == 'null'){
                            logs("<b>Menambahkan Barang Masuk</b> : Menambah barang masuk dengan nomor transaksi {$tnc_code}");
                        }else{
                            logs("<b>Memperbarui Barang Masuk</b> : Memperbarui barang masuk dengan nomor transaksi {$tnc_code}");
                        }
                    }else{
                        if($id == 'null'){
                            logs("<b>Menambahkan Barang Keluar</b> : Menambah barang keluar dengan nomor transaksi {$tnc_code}");
                        }else{
                            logs("<b>Memperbarui Barang Keluar</b> : Memperbarui barang keluar dengan nomor transaksi {$tnc_code}");
                        }
                    }
                    r_success();
                }
            }
        }else{
            r_success();
        }
        
    }

    public function list_incoming()
    {
        if(role(['admin', 'operator'],false)){
            $this->load->view('content/admin/transaction/incoming/list_incoming');
        }else{
            $this->load->view('content/error_404');
        }
    }

    public function incoming_table()
    {
        $id = 'incoming_id';
        $table = 'incoming';
        $column = array(
            'incoming_id',
            'inc_code',
            'inc_date',
            'user_id',
            'user_updated'
        );
        show_table($id, $table, $column, 'null');
    }

    public function outgoing($id)
    {
        if(role(['admin', 'operator'],false)){
            $data['id'] = $id;
            if($id == 'null'){
                $status = $this->main_model->limitWhere(0, 1,'outgoing', 'status', 1, 'outgoing_id');
                if($status){
                    $data['otg_code'] = $status[0]['otg_code'];
                    $data['date'] = to_date_bootstrap($status[0]['otg_date']);
                    $data['status'] = 1;
                    $data['outgoing_list'] = unserialize($status[0]['outgoing']);
                }else{
                    $data['otg_code'] = generate_code_tnc('outgoing');
                    $data['date'] = date('d-m-Y');
                    $data['status'] = 0;
                    $data['outgoing_list'] = array();
                }
            }else{
                $outgoing = $this->main_model->gda3p('outgoing', 'outgoing_id', $id);
                $data['otg_code'] = $outgoing[0]['otg_code'];
                $data['date'] = to_date_bootstrap($outgoing[0]['otg_date']);
                $data['status'] = 1;    
                $data['outgoing_list'] = unserialize($outgoing[0]['outgoing']);
            }
            $this->load->view('content/admin/transaction/outgoing/index', $data);
        }else{
            $data['to_show'] = '#outgoing_normal';
            $data['to_hide'] = '#outgoing_spin';
            $this->load->view('content/error_404', $data);
        }
    }

    public function list_outgoing()
    {
        if(role(['admin', 'operator'],false)){
            $this->load->view('content/admin/transaction/outgoing/list_outgoing');
        }else{
            $this->load->view('content/error_404');
        }
    }

    public function outgoing_table()
    {
        $id = 'outgoing_id';
        $table = 'outgoing';
        $column = array(
            'outgoing_id',
            'otg_code',
            'otg_date',
            'user_id',
            'user_updated'
        );
        show_table($id, $table, $column, 'null');
    }

}
