<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        guest();
    }

    public function index()
    {
        $this->load->view('auth/signin');
    }

    public function forgot_password()
    {
        $this->load->view('auth/forgot_password');
    }

    public function signinProccess()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;
        $password = md5($obj->password);
        $user = $this->main_model->gda5p('users', 'email', $email, 'password', $password);
        if ($user) {
            $session_array = array();
            foreach ($user as $key => $val) {
                $session_array = array(
                    'id' => $val['user_id'],
                    'name' => $val['name'],
                    'email' => $val['email'],
                    'role' => $this->main_model->gdo4p('roles','name', 'role_id', $val['role_id']),
                );
                $this->main_model->destroy('token_password', 'email', $val['email']);
            }
            $this->session->set_userdata('inv_in', $session_array);
            r_success();
        }
    }

    public function send_token()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;
        $data['token'] = random_string(40);
        $data['email'] = $email;
        $url = base_url() . 'password_reset/' . $data['token'];
        $message = 'Ini adalah link reset password anda : ' . $url;
        $send_mail = send_mail($email, $message, 'Password reset');
        if ($send_mail) {
            $insert = $this->main_model->store('token_password', $data);
            r_success();
        }
    }

    public function password_reset($token)
    {
        $check = $this->main_model->gda3p('token_password', 'token', $token);
        if (!$check) {
            $this->load->view('error_404');
        } else {
            $data['email'] = $check[0]['email'];
            $this->load->view('auth/password_reset', $data);
        }
    }

    public function reset()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;
        $data['password'] = md5($obj->password);
        $reset = $this->main_model->update('users', $data, 'email', $email);
        if ($reset) {
            $this->main_model->destroy('token_password', 'email', $email);
            r_success();
        }
    }

    public function check_email()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;
        $check = $this->main_model->gda3p('users', 'email', $email);
        if ($check) {
            r_success();
        }
    }

    public function check_token()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;
        $token = $this->main_model->gda3p('token_password', 'email', $email);
        if (!$token) {
            r_success();
        }
    }

}
