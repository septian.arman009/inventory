<?php
defined('BASEPATH') or exit('No direct script access allowed');

class D_table extends CI_Model
{

    public function Datatables($dt)
    {
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        
        if($dt['table'] == 'incoming' || $dt['table'] == 'outgoing'){
            $sql = "SELECT {$columns} FROM {$dt['table']} WHERE status = 0";
        }else{
            $sql = "SELECT {$columns} FROM {$dt['table']}";
        }
        
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        $columnd = $dt['col-display'];
        $count_c = count($columnd);
        $search = $dt['search']['value'];
        $where = '';
        if ($search != '') {
            for ($i = 0; $i < $count_c; $i++) {
                $where .= $columnd[$i] . ' LIKE "%' . $search . '%"';

                if ($i < $count_c - 1) {
                    if($dt['table'] == 'incoming' || $dt['table'] == 'outgoing'){
                        $where .= ' AND ';
                    }else{
                        $where .= ' OR ';
                    }
                }
            }
        }
        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                if($dt['table'] == 'incoming' || $dt['table'] == 'outgoing'){
                    if($i == 2){
                        $where = $columnd[$i] . "='" . to_date_mysql($searchCol) . "'";
                    }else if ($i == 3 || $i == 4) {
                        $users = $this->db->query('select user_id from users where name like "%' . $searchCol . '%"')->result_array();
                        $in = takeArrayValueToString($users, 'user_id');
                        if ($in != '') {
                            $where = $columnd[$i] . ' IN (' . $in . ')';
                        } else {
                            $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                        }
                    }else{
                        $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" '; 
                    }
                } else if($dt['table'] == 'items'){
                   if ($i == 2) {
                        $categories = $this->db->query('select c_code from categories where name like "%' . $searchCol . '%"')->result_array();
                        $in = takeArrayValueToString($categories, 'c_code');
                        if ($in != '') {
                            $where = $columnd[$i] . ' IN (' . $in . ')';
                        } else {
                            $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                        }
                    }else{
                        $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" '; 
                    }
                }else{
                    $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                }
                break;
            }
        }
        if ($where != '') {
            if(
                $dt['table'] == 'incoming' || $dt['table'] == 'outgoing'
            ){
                $sql .= " AND " . $where;
            }else{
                $sql .= " WHERE " . $where;
            }
        }
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        $list = $this->db->query($sql);
        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        $no = 1;
        foreach ($list->result_array() as $row => $val) {
            $rows = array();

            if (
                $dt['table'] == 'users' || $dt['table'] == 'types' || $dt['table'] == 'categories'
                || $dt['table'] == 'items' || $dt['table'] == 'supliers' || $dt['table'] == 'customers'
                || $dt['table'] == 'incoming' || $dt['table'] == 'outgoing'
            ) {
                $id = 0;
                $data = 1;
                foreach ($dt['col-display'] as $key => $kolom) {
                    if($dt['table'] == 'incoming' || $dt['table'] == 'outgoing'){
                        if ($id == 0) {
                            $id = $val[$kolom];
                            $rows[] = $no;
                        } else if($data == 4 || $data == 5){
                            $rows[] = $this->gdo4p('users', 'name', 'user_id', $val[$kolom]);
                        }else {
                            $rows[] = $val[$kolom];
                        }
                        
                    }else if($dt['table'] == 'items'){
                        if ($id == 0) {
                            $id = $val[$kolom];
                            $rows[] = $no;
                        } else if($data == 3){
                            $rows[] = $this->gdo4p('categories', 'name', 'c_code', $val[$kolom]);
                        }else {
                            $rows[] = $val[$kolom];
                        }
                        
                    }else{
                        if ($id == 0) {
                            $id = $val[$kolom];
                            $rows[] = $no;
                        } else {
                            $rows[] = $val[$kolom];
                        }
                    }
                    $data++;
                }

                if($dt['table'] == 'incoming'){
                    $rows[] =
                    '<a id="edit" title="Edit" class="btn btn-info btn-xs waves-effect" 
                    onclick="edit(' . "'" . $id . "'" . ');""><i class="fa fa-pencil-square-o"></i></a>
                    | <a id="destroy" title="Hapus" class="btn btn-danger btn-xs waves-effect"
                    onclick="destroy(' . "'" . $id . "'" . ');"><i class="fa fa-trash-o"></i></a>
                    | <a id="print" title="Cetak" class="btn btn-success btn-xs waves-effect"
                    href="printIncoming/' . $id  . '" target="_blank"><i class="fa fa-print"></i></a>';
                }else if($dt['table'] == 'outgoing'){
                    $rows[] =
                    '<a id="edit" title="Edit" class="btn btn-info btn-xs waves-effect" 
                    onclick="edit(' . "'" . $id . "'" . ');""><i class="fa fa-pencil-square-o"></i></a>
                    | <a id="destroy" title="Hapus" class="btn btn-danger btn-xs waves-effect"
                    onclick="destroy(' . "'" . $id . "'" . ');"><i class="fa fa-trash-o"></i></a>
                    | <a id="print" title="Cetak" class="btn btn-success btn-xs waves-effect"
                    href="printOutgoing/' . $id  . '" target="_blank"><i class="fa fa-print"></i></a>';
                }else{
                    $rows[] =
                    '<a id="edit" title="Edit" class="btn btn-info btn-xs waves-effect" 
                    onclick="edit(' . "'" . $id . "'" . ');""><i class="fa fa-pencil-square-o"></i></a>
                    | <a id="destroy" title="Hapus" class="btn btn-danger btn-xs waves-effect"
                    onclick="destroy(' . "'" . $id . "'" . ');"><i class="fa fa-trash-o"></i></a>';
                }
                
            }

            $no++;
            $option['data'][] = $rows;

        }
        echo json_encode($option);
    }

    public function gdo4p($table, $take, $column, $id)
    {
        $this->db->where($column, $id);
        $query = $this->db->get($table)->result_array();
        if ($query) {
            if ($query[0][$take]) {
                return $query[0][$take];
            }
        }

    }

}
