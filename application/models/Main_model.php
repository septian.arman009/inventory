<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main_model extends CI_Model
{

    public function gda1p($table)
    {
        return $this->db->get($table)->result_array();
    }

    public function gda1po($table, $order_by, $flow)
    {
        return $this->db->query("select * from $table order by $order_by $flow")->result_array();
    }

    public function gda3p($table, $column, $id)
    {
        $this->db->where($column, $id);
        return $this->db->get($table)->result_array();
    }

    public function gda3pl($table, $column, $key, $order, $flow)
    {
        $this->db->like($column, $key, 'both');
        $this->db->order_by($order, $flow);
        return $this->db->get($table)->result_array();
    }

    public function gda3pwl($table, $column, $key, $column1, $key1, $order, $flow)
    {
        $this->db->where($column, $key);
        $this->db->like($column1, $key1, 'both');
        $this->db->order_by($order, $flow);
        return $this->db->get($table)->result_array();
    }

    public function gda5p($table, $column, $id, $column1, $id1)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        return $this->db->get($table)->result_array();
    }

    public function gda5pg($table, $column, $id, $column1, $id1, $order_by, $flow)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        $this->db->order_by($order_by, $flow);
        return $this->db->get($table)->result_array();
    }

    public function gda7p($table, $column, $id, $column1, $id1, $column2, $id2)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        $this->db->where($column2, $id2);
        return $this->db->get($table)->result_array();
    }

    public function gdo4p($table, $take, $column, $id)
    {
        $this->db->where($column, $id);
        $query = $this->db->get($table)->result_array();
        if ($query) {
            if ($query[0][$take]) {
                return $query[0][$take];
            } else {
                return 0;
            }
        }
    }

    public function update($table, $data, $column, $id)
    {
        $this->db->where($column, $id);
        return $this->db->update($table, $data);
    }

    public function updateSingleColumn($table, $target, $value, $column, $key)
    {   
        $query = $this->db->query("update $table set $target=$value where $column = '$key'");
        return $query;
    }

    public function update6p($table, $data, $column, $id, $column1, $id1)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        return $this->db->update($table, $data);
    }

    public function update8p($table, $data, $column, $id, $column1, $id1, $column2, $id2)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        $this->db->where($column2, $id2);
        return $this->db->update($table, $data);
    }

    public function update_add1($add, $table, $column1, $where, $key)
    {
        $query = $this->db->query("update $table set $column1=$column1+$add where $where = '$key'");
        return $query;
    }

    public function update_add2($add, $total, $table, $column1, $column2, $where, $key)
    {
        $query = $this->db->query("update $table set $column1=$column1+$add,  $column2=$column2+$total where $where = '$key'");
        return $query;
    }

    public function store($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    public function truncate($table)
    {
        return $this->db->query("truncate $table");
    }

    public function destroy($table, $column, $id)
    {
        $this->db->where($column, $id);
        return $this->db->delete($table);
    }

    public function max($tabel, $kolom)
    {
        $query = $this->db->query("select max($kolom)$kolom from $tabel")->result_array();
        if ($query) {
            if ($query[0][$kolom]) {
                return $query[0][$kolom];
            }
        }
    }

    public function max_all($tabel, $kolom)
    {
        $query = $this->db->query("select * from $tabel where $kolom=(select max($kolom) from $tabel)")->result_array();
        return $query;
    }

    public function last_where($table, $id, $column, $data)
    {
        $query = $this->db->query("select * from $table where $id = (select max($id) from $table where $column='$data')")->result_array();
        return $query;
    }

    public function count($table, $id)
    {
        $query = $this->db->query("select count($id) as total_row from $table")->result_array();
        if ($query) {
            if ($query[0]['total_row'] != '') {
                return $query[0]['total_row'];
            } else {
                return 0;
            }
        }
    }

    public function counttable($table)
    {
        $query = $this->db->query("select count(*) as total_row from $table")->result_array();
        return $query[0]['total_row'];
    }

    public function countlike($table, $id, $column, $like)
    {
        $query = $this->db->query("select count($id) as total_row from $table where $column like '%$like%'")->result_array();
        if ($query) {
            if ($query[0]['total_row'] != '') {
                return $query[0]['total_row'];
            } else {
                return 0;
            }
        }
    }

    public function countwhere($table, $id, $column, $data)
    {
        $query = $this->db->query("select count($id) as total_row from $table
        where $column = '$data'")->result_array();
        if ($query) {
            if ($query[0]['total_row'] != '') {
                return $query[0]['total_row'];
            } else {
                return 0;
            }
        }
    }

    public function count2where($table, $id, $column, $data, $column1, $data1)
    {
        $query = $this->db->query("select count($id) as total_row from $table
        where $column='$data' and $column1='$data1'")->result_array();
        if ($query) {
            if ($query[0]['total_row'] != '') {
                return $query[0]['total_row'];
            } else {
                return 0;
            }
        }
    }

    public function countgroup($table, $id)
    {
        $query = $this->db->query("select $id as total_row from $table group by $id")->result_array();
        if ($query) {
            if ($query[0]['total_row'] != '') {
                return count($query);
            } else {
                return 0;
            }
        }
    }

    public function count3where($table, $id, $column, $data, $column1, $data1, $column2, $data2)
    {
        $query = $this->db->query("select count($id) as total_row from $table
        where $column = '$data' and $column1 = '$data1' and $column2 = '$data2'")->result_array();
        if ($query) {
            if ($query[0]['total_row'] != '') {
                return $query[0]['total_row'];
            } else {
                return 0;
            }
        }
    }

    public function sumwhere($tabel, $take, $column, $data)
    {
        $query = $this->db->query("select sum($take) as total from $tabel
        where $column = '$data'")->result_array();
        if ($query) {
            if ($query[0]['total'] != '') {
                return $query[0]['total'];
            } else {
                return 0;
            }
        }
    }

    public function sum2where($tabel, $take, $column, $data, $column1, $data1)
    {
        $query = $this->db->query("select sum($take) as total from $tabel
        where $column = '$data' and $column1 = '$data1'")->result_array();
        if ($query) {
            if ($query[0]['total'] != '') {
                return $query[0]['total'];
            } else {
                return 0;
            }
        }
    }

    public function sumin2where($tabel, $take, $column, $in, $column1, $data1, $column2, $data2)
    {
        $query = $this->db->query("select sum($take) as total from $tabel
		where $column in ($in) and $column1 = '$data1' and $column2 = '$data2'")->result_array();

        if ($query) {
            if ($query[0]['total'] != '') {
                return $query[0]['total'];
            } else {
                return 0;
            }
        }
    }

    public function in3p($table, $column, $in)
    {
        return $query = $this->db->query("select * from $table where $column in ($in)")->result_array();
    }

    public function in7p($table, $column, $in, $column1, $data1, $colum2, $data2)
    {
        return $query = $this->db->query("select * from $table where $column in ($in) and $column1 = '$data1' and $colum2 = '$data2'")->result_array();
    }

    public function notin3p($table, $column, $in)
    {
        return $query = $this->db->query("select * from $table where $column not in ($in)")->result_array();
    }

    public function limit($start, $content_per_page, $table, $column_id)
    {
        $sql = "select * from  $table order by $column_id desc LIMIT $start,$content_per_page";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function limitWhere($start, $content_per_page, $table, $column, $key, $column_id)
    {
        $sql = "select * from  $table where $column like '%$key%' order by $column_id desc LIMIT $start,$content_per_page";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function limitIn($start, $content_per_page, $table, $column, $key)
    {
        $sql = "select * from  $table where $column in ($key) order by id desc LIMIT $start,$content_per_page";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function limitWhereNotLike($start, $content_per_page, $table, $column, $key)
    {
        $sql = "select * from  $table where $column = '$key' order by id desc LIMIT $start,$content_per_page";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function limitWhereNotLikeOrder($start, $content_per_page, $table, $column, $key, $order)
    {
        $sql = "select * from  $table where $column = '$key' order by '$order' LIMIT $start,$content_per_page";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function last_ai($table)
    {
        $query = $this->db->query("SHOW TABLE STATUS LIKE '$table'")->result_array();
        return $query[0]['Auto_increment'];
    }

    public function exixt5p($table, $column, $table1, $column1, $id)
    {
        $query = $this->db->query("select * from $table where $column = (select $column from $table1 where $column1 = $id limit 1)")->result_array();
        return $query;
    }

    function updateMultiple($table, $data, $column)
    {
        $query = $this->db->update_batch($table, $data, $column);
        return $query;
    }

    function whereIn($table, $column, $data)
    {
        $this->db->where_in($column, $data);
        return $this->db->get($table)->result_array();
    }

    function between($table, $column, $start, $end)
    {
        $query = $this->db->query("SELECT * FROM $table WHERE $column BETWEEN '$start' AND '$end'")->result_array();
        return $query;
    }
}
