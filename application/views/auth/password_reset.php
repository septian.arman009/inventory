<?php $this->load->view('auth/layout/header')?>
<div class="login-box animated fadeInDown">
	<div class="login-body">
		<div class="login-title">
			<strong>Masukan password baru</strong>
		</div>
		<form class="form-horizontal">
			<div class="form-group">
				<div class="col-md-12">
					<input onkeyup="check_password()" id="password" type="password" class="form-control" placeholder="Password" />
					<p id="passless" class="btn btn-link btn-block" style="display:none;color:white;">Password minimal 8 karakter !</p>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<input onkeyup="check_password()" id="confirm" type="password" class="form-control" placeholder="Confirm Password" />
					<p id="conless" class="btn btn-link btn-block" style="display:none;color:white;">Password minimal 8 karakter !</p>
					<p id="notmatch" class="btn btn-link btn-block" style="display:none;color:white;">Password tidak sama !</p>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6">
					<a href="<?php echo base_url() ?>signin" class="btn btn-link btn-block">Kemabali ke Signin</a>
				</div>
				<div class="col-md-6">
					<a id="reset" onclick="reset()" class="btn btn-info btn-block">Reset Password</a>
					<a style="display:none" id="loading" class="btn btn-info btn-block">Mohon Tunggu ..</a>
				</div>
			</div>
		</form>
	</div>
	<div class="login-footer">
		<div class="pull-left">
			&copy; 2019 Inventory System
		</div>
		<div class="pull-right">
		</div>
	</div>
</div>
<?php $this->load->view('auth/layout/footer')?>

<script id="forgotjs">
	function reset() {
		$("#reset").hide();
		$("#loading").show();
		var data = {
			email: '<?php echo $email ?>',
			password: $("#password").val()
		};

		postData("<?php echo base_url() ?>auth_controller/reset", data, function (err, response) {
			if (response) {
				if (response.status == "success") {
					swal("Sukses", "Berhasil mereset password.", "success");
					setTimeout(() => {
						window.location = '<?php echo base_url() ?>signin';
					}, 1000);
				} else {
					swal("Terjadi Kesalahan", "Gagal mereset password.", "error");
				}
			}
		});
	}
	document.getElementById('forgotjs').innerHTML = "";
</script>