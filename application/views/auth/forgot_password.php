<?php $this->load->view('auth/layout/header')?>
<div class="login-box animated fadeInDown">
	<div class="login-body">
		<div class="login-title">
			<strong>Masukan email anda !</strong>
		</div>
		<form class="form-horizontal">
			<div class="form-group">
				<div class="col-md-12">
					<input onkeyup="check()" id="email" type="email" class="form-control" placeholder="Email" />
					<p id="invalid_email" class="btn btn-link btn-block" style="display:none;color:white;">Masukan format email dengan benar !</p>
					<p id="notexist" class="btn btn-link btn-block" style="display:none;color:white;">Email tidak terdaftar !</p>
					<p id="token-exist" class="btn btn-link btn-block" style="display:none;color:white;">Token sudah dikirim, silakan cek email anda !</p>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6">
					<a href="<?php echo base_url() ?>signin" class="btn btn-link btn-block">Kembali ke Signin</a>
				</div>
				<div class="col-md-6">
					<a disabled id="send_token" onclick="send_token()" class="btn btn-info btn-block">Kirim Token</a>
					<a style="display:none" id="loading" class="btn btn-info btn-block">Mengirim ..</a>
				</div>
			</div>
		</form>
	</div>
	<div class="login-footer">
		<div class="pull-left">
			&copy; 2019 Inventory System
		</div>
		<div class="pull-right">
		</div>
	</div>
</div>
<?php $this->load->view('auth/layout/footer')?>