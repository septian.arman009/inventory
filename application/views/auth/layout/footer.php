</div>
</body>
<!-- START PRELOADS -->
<audio id="audio-alert" src="<?php echo base_url() ?>assets/admin/audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="<?php echo base_url() ?>assets/admin/audio/fail.mp3" preload="auto"></audio>
<!-- END PRELOADS -->

<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/js/plugins/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/js/plugins/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/js/plugins.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/js/actions.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/myjs/auth.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/myjs/main.js"></script>
<script src="<?php echo base_url() ?>assets/admin/sweetalert/sweetalert.min.js"></script>

</html>