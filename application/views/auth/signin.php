<?php $this->load->view('auth/layout/header')?>
<div class="login-box animated fadeInDown">
	<!-- <div class="login-logo"></div> -->
	<div class="login-body">
		<div class="login-title">
			<strong>Selamat Datang</strong>
		</div>
		<form class="form-horizontal">
			<div class="form-group">
				<div class="col-md-12">
					<input id="email" type="email" class="form-control" placeholder="Email" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<input id="password" type="password" class="form-control" placeholder="Password" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6">
					<a href="<?php echo base_url() ?>forgot_password" class="btn btn-link btn-block">Lupa password ?</a>
				</div>
				<div class="col-md-6">
					<a onclick="signin()" class="btn btn-info btn-block" id="signin">Signin</a>
				</div>
			</div>
		</form>
	</div>
	<div class="login-footer">
		<div class="pull-left">
			&copy; 2019 Inventory System
		</div>
		<div class="pull-right">
		</div>
	</div>
</div>

<script id="signinjs">
	var input = document.getElementById("email");

	input.addEventListener("keyup", function (event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("signin").click();
		}
	});

	var input1 = document.getElementById("password");

	input1.addEventListener("keyup", function (event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("signin").click();
		}
	});

	document.getElementById('signinjs').innerHTML = "";
</script>

<?php $this->load->view('auth/layout/footer')?>