<div class="panel-body">
	<div class="alert alert-danger" role="alert">
		<strong>Error !</strong> Terjadi Kesalahan Hak Akses.
	</div>
</div>

<script>
	$(document).ready(function () {
		var to_show = '<?php echo $to_show ?>';
		var to_hide = '<?php echo $to_hide ?>';
		$(to_show).show();
		$(to_hide).hide();
	});
</script>