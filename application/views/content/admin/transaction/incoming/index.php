<ul class="breadcrumb">
	<li>
		<a href="#">Transaksi</a>
	</li>
	<li class="active">Barang Masuk</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">
				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 style="font-weight: bold" class="panel-title">
							Kode Transaksi : <a id="temp_code"><?php echo $inc_code ?></a>
						</h3>
					</div>
					<?php if($id != 'null'){?>
						<a onclick="loadView('transaction_controller/list_incoming', '.content')" class="btn btn-default">
							<i class="fa fa-arrow-left"></i>
						</a>
					<?php } ?>
					<div class="panel-body">
						Form Barang Masuk
						<?php if($id == 'null'){?>
							<a onclick="loadView('transaction_controller/list_incoming', '.content')" class="btn btn-warning pull-right">Daftar Barang Masuk</a>
						<?php } ?>
					</div>

					<input style="display: none" id="inc_code" type="text" class="form-control">

					<div class="panel-body form-group-separated">
						<div class="form-group">
							<label class="col-md-3 col-xs-3 control-label">Nama Barang</label>
							<div class="col-md-6 col-xs-6">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-dropbox"></span>
									</span>
									<input onchange="getItem($(this).val())" id="item" type="text"
										class="typehead form-control">
								</div>
							</div>
						</div>
					</div>

					<div class="panel-body form-group-separated">
						<div class="form-group">
							<label class="col-md-3 col-xs-3 control-label">Detail</label>
							<div class="col-md-6 col-xs-6">
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="contact-info" id="detail">
										<p><small>Nama Barang</small><br></p>
										<p><small>Kode Barang</small><br></p>
										<p><small>Jenis Barang</small><br></p>
										<p><small>Kategori</small><br></p>
										<p><small>Suplier</small><br></p>
										<p><small>Stock</small><br></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="panel-body form-group-separated">
						<div class="form-group">
							<label class="col-md-3 col-xs-3 control-label">Jumlah Barang Masuk</label>
							<div class="col-md-6 col-xs-6">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-unsorted"></span>
									</span>
									<input onkeyup="add_form()" id="qty" type="number" class="form-control">
								</div>
							</div>
						</div>
					</div>

					<div class="panel-body form-group-separated">
						<div class="form-group">
							<label class="col-md-3 col-xs-3 control-label">Tangga Barang Masuk</label>
							<div class="col-md-6 col-xs-6">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
									<input onchange="generate_code()" readonly="" style="color: black; cursor: pointer;" onchange="check_form()" id="inc_date" value="<?php echo $date ?>"
									 type="text" class="form-control datepicker" data-date="<?php echo date('d-m-Y') ?>">
								</div>
							</div>
						</div>
					</div>

					<div class="panel-body form-group-separated"></div>

					<div class="panel-body">
						<table class="table stripe hover">
							<thead>
								<tr>
									<th id="th" width="10%">No</th>
									<th id="th" width="20%">Kode Barang</th>
									<th id="th" width="20%">Kategori Barang</th>
									<th id="th" width="20%">Nama Barang</th>
									<th id="th" width="20%">Suplier</th>
									<?php if($id == 'null'){ ?>
										<th id="th" width="10%">Stock Saat Ini</th>
									<?php }else{ ?>
										<th id="th" width="10%">Stock Awal</th>
									<?php } ?>
									<th id="th" width="10%">Barang Masuk</th>
									<th id="th" width="10%">Total Stock</th>
									<th id="th" width="5%"></th>
								</tr>
							</thead>
							<tbody>
								<?php $no = 1; foreach ($incoming_list as $key => $value) { 
									$stock = $this->main_model->gdo4p('items', 'qty', 'i_code', $value['i_code']);
									$suplier = $this->main_model->gdo4p('supliers', 'name', 's_code', $value['s_code']);
									?>
									<tr>
										<td><?php echo $no++ ?></td>
										<td><?php echo $value['i_code'] ?></td>
										<td><?php 
											$c_code = $this->main_model->gdo4p('items', 'c_code', 'i_code', $value['i_code']);
											$category = $this->main_model->gdo4p('categories', 'name', 'c_code', $c_code);

											echo $category;
										?></td>
										<td><?php echo $value['name'] ?></td>
										<td><?php echo "{$suplier} <p>{$value['s_code']}</P" ?></td>
										<?php if($id == 'null'){ ?>
											<td><?php echo $stock ?></td>
											<td><?php echo $value['qty'] ?></td>
											<td><?php echo $stock + $value['qty'] ?><p style="color: red;">Jumlah setelah disimpan.</p></td>
										<?php }else{ ?>
											<?php if($value['status'] == 0) {?>
												<td><?php echo $stock - $value['qty'] ?></td>
												<td><?php echo $value['qty'] ?></td>
												<td><?php echo $stock ?></td>
											<?php }else{ ?>
												<td><?php echo $stock ?></td>
												<td><?php echo $value['qty'] ?></td>
												<td><?php echo $stock + $value['qty'] ?><p style="color: red;">Jumlah setelah disimpan.</p></td>
											<?php } ?>
										<?php } ?>
										<td>
											<a title="Hapus" class="btn btn-danger btn-xs waves-effect" onclick="removeItem('<?php echo $value['i_code'] ?>');">
												<i class="fa fa-trash-o"></i>
											</a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>

					<div class="panel-footer">
						<a disabled id="add" onclick="addItem()" class="btn btn-success">Tambah Ke List</a>
						<a style="display:none" id="loading_add" class="btn btn-success">Mohon Tunggu ..</a>
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right"></a>
						<a style="display:none" id="loading_save" class="btn btn-primary pull-right">Mohon Tunggu ..</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="incomingjs">
	$(document).ready(function () {
		$("#incoming_spin").hide();
		$("#incoming_normal").show();

		var status = '<?php echo $status ?>';
		var id = '<?php echo $id ?>';

		if(status == 0){
			$(".datepicker").datepicker({
				format: 'dd-mm-yyyy'
			});
			generate_code();
		}else{
			$("#inc_code").val('<?php echo $inc_code ?>');
			$("#save").removeAttr("disabled");
		}

		if(id == 'null'){
			$("#save").html('Simpan');
		}else{
			$("#save").html('Update');
		}

		$('#item').typeahead({
			source: function (query, process) {
				return $.get('transaction_controller/search/items', {
					query: query
				}, function (data) {
					data = $.parseJSON(data);
					return process(data);
				});
			}
		});
		
		this.item_code = 'false';
	});

	function add_form() {
		if(this.item_code != 'false' && $("#qty").val() != ''){
			$("#add").removeAttr("disabled");
		}else{
			$("#add").attr("disabled", "disabled");
		}
	}

	function getItem(item) {
		data = {
			id: item,
			column: 'name',
			table: 'items'
		}

		postData('main_controller/get_data', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					$("#detail").html(
						'<p><small>Nama Barang</small><br>'+ response.data[0].name +'</p>' +
						'<p><small>Kode Barang</small><br>'+ response.data[0].i_code +'</p>' +
						'<p><small>Jenis Barang</small><br>'+ response.data[0].t_code +'</p>' +
						'<p><small>Kategori</small><br>'+ response.data[0].c_code +'</p>' +
						'<p><small>Suplier</small><br>'+ response.data[0].s_code +'</p>' +
						'<p><small>Stock</small><br>'+ response.data[0].qty +'</p>'
					);
					this.item_code = response.data[0].i_code;
					setTimeout(() => {
						add_form();
					}, 100);
				}else{
					$("#detail").html(
						'<p><small>Nama Barang</small><br></p>' +
						'<p><small>Kode Barang</small><br></p>' +
						'<p><small>Jenis Barang</small><br></p>' +
						'<p><small>Kategori</small><br></p>' +
						'<p><small>Suplier</small><br></p>' +
						'<p><small>Stock</small><br></p>'
					);
					this.item_code = 'false';
					setTimeout(() => {
						add_form();
					}, 100);
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function addItem() {
		$("#add").hide();
		$("#loading_add").show();
		data = {
			tnc_code: $("#inc_code").val(),
			tnc_table: 'incoming',
			tnc_column: 'inc_code',
			i_code: this.item_code,
			qty: $("#qty").val(),
			tnc_date: $("#inc_date").val()
		}

		postData('transaction_controller/addItem/<?php echo $id ?>', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					$("#add").show();
					$("#loading_add").hide();
					swal("Sukses", "Data barang berhasil disimpan di list Incoming dengan nomor transaksi : "+$("#inc_code").val(), "success");
					loadView('transaction_controller/incoming/<?php echo $id ?>', '.content');
				}else{
					$("#add").show();
					$("#loading_add").hide();
					swal("Terjadi Kesalahan", "Barang sudah didaftarkan, silakan hapus barang terlebih dahulu.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function removeItem(i_code) {
		swal({
				title: "Apakah anda yakin ?",
				text: "Hapus barang : "+i_code+" ?",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, Hapus Sekarang",
				closeOnConfirm: false
			},
			function () {
				var data = {
					tnc_table: 'incoming',
					tnc_column: 'inc_code',
					tnc_code: $("#inc_code").val(),
					i_code: i_code
				}
				postData('transaction_controller/removeItem/<?php echo $id ?>', data, function (err, response) {
					if (response) {
						if (response.status == 'success') {
							swal("Sukses", "Berhasil menghapus data barang dari list.", "success");
							loadView('transaction_controller/incoming/<?php echo $id ?>', '.content');
						}else{
							swal("Terjadi Kesalahan", "Minimal 1 barang masuk pada list.", "error");
						}
					} else {
						console.log('ini error : ', err);
					}
				});
			}
		);
	}

	function action() {
		$("#save").hide();
		$("#loading_save").show();
		data = {
			tnc_table: 'incoming',
			tnc_column: 'inc_code',
			tnc_code: $("#inc_code").val()
		}

		postData('transaction_controller/saveTnc/<?php echo $id ?>', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					$("#save").show();
					$("#loading_save").hide();
					swal("Sukses", "Barang masuk berhasil disimpan dengan nomor transaksi : "+$("#inc_code").val(), "success");
					loadView('transaction_controller/incoming/<?php echo $id ?>', '.content');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function generate_code() {
		var date = $("#inc_date").val();
		var name_code = '';
		var array_date = date.split('-');		
		var middle_code = array_date[0]+array_date[1]+''+array_date[2].slice(2,4);
			
		var inc_code = 'INC/'+middle_code+'/'+'<?php echo $inc_code ?>';
		
		$("#inc_code").val(inc_code);
		$("#temp_code").html(inc_code);
	}

	document.getElementById('incomingjs').innerHTML = "";
</script>