<ul class="breadcrumb">
	<li>
		<a href="#">Transaksi</a>
	</li>
	<li>Barang Masuk</li>
	<li class="active">Daftar Barang Masuk</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar Barang Masuk</h3>
				</div>
				<a onclick="loadView('transaction_controller/incoming/null', '.content')" class="btn btn-default">
					<i class="fa fa-arrow-left"></i>
				</a>
				<div class="panel-body">
					<table id="listinc-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">No</th>
								<th id="th">Kode Barang Masuk</th>
								<th id="th">Tanggal Barang Masuk</th>
								<th id="th">Dibuat Oleh</th>
                                <th id="th">Diperbarui Oleh</th>                                
								<th id="th" class="no-sort" width="15%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">no</th>
								<th class="footer">Kode Barang Masuk</th>
								<th class="footer">DD-MM-YYYY</th>
                                <th class="footer">Nama</th>
                                <th class="footer">Nama</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->
		</div>
	</div>
</div>

<script id="listincomingjs">
	$(document).ready(function () {
		$('#listinc-table tfoot th').each(function () {
			var title = $(this).text();
			if (title == 'DD-MM-YYYY') {
                var inp = '<input readonly style="cursor: pointer; color: black;" type="text" class="form-control footer-s datepicker" placeholder="' + title + '" id="' +
                title + ' data-date="<?php echo date("Y-m-d") ?>" />';
			} else {
				var inp = '<input type="text" class="form-control footer-s" placeholder="' + title + '" id="' + title + '" />';
			}
			$(this).html(inp);
		});

		var table = $('#listinc-table').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": 'transaction_controller/incoming_table',
				"type": "POST"
			}
		});

		table.columns().every(function () {
			var that = this;
			$('input', this.footer()).on('keyup change', function () {
				if (that.search() !== this.value) {
					that.search(this.value).draw();
				}
			});
        });
        
        $(".datepicker").datepicker({
			format: 'dd-mm-yyyy'
		});

		$("#no").hide();
	});

	function destroy(id) {
		swal({
				title: "Apakah anda yakin ?",
				text: "Menghapus transaksi barang masuk akan mengembalikan stock pada tabel barang.",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Ya, Hapus Sekarang!",
				closeOnConfirm: false
			},
			function () {
				$(".confirm").attr('disabled', 'disabled');
				$(".cancel").attr('disabled', 'disabled');

				var data = {
					id: id
				}
				postData('main_controller/destroy/incoming/incoming_id', data, function (err, response) {
					if (response) {
						if (response.status == 'success') {
							loadView('transaction_controller/list_incoming', '.content');
							swal("Sukses", "Berhasil menghapus data barang masuk.", "success");
							$(".confirm").removeAttr('disabled');
							$(".cancel").removeAttr('disabled');
						}
					} else {
						console.log('ini error : ', err);
					}
				});
			}
		);
	}

	function edit(id) {
		loadView('transaction_controller/incoming/' + id, '.content');
	}

	document.getElementById('listincomingjs').innerHTML = "";
</script>

<style>
	#listinc-table_filter {
		display: none;
	}
</style>