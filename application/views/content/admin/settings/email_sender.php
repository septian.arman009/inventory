<ul class="breadcrumb">
	<li>
		<a href="#">Pengaturan</a>
	</li>
	<li class="active">Email Pengirim Pesan</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<form class="form-horizontal">
				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong>Email</strong> Pengirim</h3>
					</div>

					<div class="panel-body">
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Email</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-envelope"></span>
									</span>
									<input onkeyup="check()" id="send_mail" autocomplete="new-email" type="text" class="form-control" value="<?php echo $setting[0]['send_mail'] ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Password</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-lock"></span>
									</span>
									<input onkeyup="check()" id="send_pass" autocomplete="new-password" type="password" class="form-control" value="<?php echo $setting[0]['send_pass'] ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Protocol</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-gear"></span>
									</span>
									<input onkeyup="check()" id="protocol" type="text" class="form-control" value="<?php echo $setting[0]['protocol'] ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Smtp Host</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-gear"></span>
									</span>
									<input onkeyup="check()" id="smtp_host" type="text" class="form-control" value="<?php echo $setting[0]['smtp_host'] ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Smtp Port</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-gear"></span>
									</span>
									<input onkeyup="check()" id="smtp_port" type="number" class="form-control" value="<?php echo $setting[0]['smtp_port'] ?>">
								</div>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right">Simpan</a>
						<a style="display:none" id="loading" class="btn btn-primary pull-right">Mohon Tunggu ..</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>


<script id="emailsenderjs">
	$(document).ready(function () {
		$("#email_sender_spin").hide();
		$("#email_sender_normal").show();

		check();
	});

	function check() {
		var email = $("#send_mail").val();
		var password = $("#send_pass").val();
		var protocol = $("#protocol").val();
		var smtp_host = $("#smtp_host").val();
		var smtp_port = $("#smtp_port").val();
		if (email != '' && password != '' && protocol != '' && smtp_host != '' && smtp_port != '') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			email: $('#send_mail').val(),
			password: $('#send_pass').val(),
			protocol: $('#protocol').val(),
			smtp_host: $('#smtp_host').val(),
			smtp_port: $('#smtp_port').val()
		}

		var message = 'Perbarui email pengiriman berhasil.';
		var error = 'Perbarui email pengiriman gagal.';

		postData('main_controller/update_email_sender', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					swal("Sukses", message, "success");
					sub_menu('#settings', '#email_sender', 'main_controller/email_sender');
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Terjadi Kesalahan", error, "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('emailsenderjs').innerHTML = "";
</script>