<ul class="breadcrumb">
	<li>
		<a href="#">Pengaturan</a>
	</li>
	<li class="active">Ganti Password</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<form class="form-horizontal">
				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong>Ganti</strong> Password</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Password Lama</label>
							<div class="col-md-6 col-xs-12">
								<input id="old_password" onchange="check_password()" type="password" class="form-control">
								<span class="help-block" id="wrong" style="display:none;">Password salah !</span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Password Baru</label>
							<div class="col-md-6 col-xs-12">
								<input disabled id="password" onkeyup="check()" type="password" class="form-control">
								<span class="help-block" id="passless" style="display:none;">Minimal 8 karakter !</span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Konfirmasi Password Baru</label>
							<div class="col-md-6 col-xs-12">
								<input disabled id="confirm" onkeyup="check()" type="password" class="form-control">
								<span class="help-block" id="conless" style="display:none;">Minimal 8 karakter !</span>
								<span class="help-block" id="notmatch" style="display:none;">Password tidak sama !</span>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right">Simpan</a>
						<a style="display:none" id="loading" class="btn btn-primary pull-right">Mohon Tunggu ..</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>


<script id="checkpasswordjs">
	$(document).ready(function () {
		$("#change_password_spin").hide();
		$("#change_password_normal").show();

		check();
	});

	function check() {
		var password = $("#password").val();
		var confirm = $("#confirm").val();
		var passlen = password.length;
		var conlen = confirm.length;

		if (passlen != '') {
			if (passlen < 8) {
				$("#passless").show();
			} else {
				$("#passless").hide();
			}
		} else {
			$("#passless").hide();
		}
		if (confirm != '') {
			if (conlen < 8) {
				$("#conless").show();
			} else {
				$("#conless").hide();
			}
		} else {
			$("#conless").hide();
		}
		if (password != '' && confirm != '') {
			if (password == confirm) {
				$("#notmatch").hide();
				if (password.length >= 8 && confirm.length >= 8) {
					$("#save").removeAttr('disabled');
				} else {
					$("#save").attr('disabled', 'disabled');
				}
			} else {
				$("#notmatch").show();
				$("#save").attr('disabled', 'disabled');
			}
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function check_password() {
		var data = {
			old_password: $('#old_password').val(),
		}

		postData('main_controller/check_old_password', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#password").removeAttr('disabled');
					$("#confirm").removeAttr('disabled');
					$("#wrong").hide();
				} else {
					$("#password").attr('disabled', 'disabled');
					$("#confirm").attr('disabled', 'disabled');
					$("#wrong").show();
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			password: $('#password').val()
		}

		var message = 'Ganti password berhasil.';
		var error = 'Ganti password gagal.';

		postData('main_controller/update_password', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					swal("Sukses", message, "success");
					sub_menu('#settings', '#change_password', 'main_controller/change_password');
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Terjadi Kesalahan", error, "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('checkpasswordjs').innerHTML = "";
</script>