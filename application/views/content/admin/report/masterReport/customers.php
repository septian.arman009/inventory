<?php

$this->fpdf->FPDF_17('P', 'cm', 'A4');
$this->fpdf->AliasNbPages();
$this->fpdf->AddPage();

$this->fpdf->Image('assets/admin/img/logo.png', 1, 1, 2.5, 0, '', base_url('admin'));

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 24);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, 'CV. HIKARI TECHNOLOGY', 0, 0, 'C');

$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.8);
$this->fpdf->Cell(0, 0, 'Jl. Raya Bosih Central Ruko Selang BLok. A15-16, Cibitung - Bekasi 17520', 0, 0, 'C');
$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, 'Telp. 021 - 8839 2521 / 0821 2249 1680', 0, 0, 'C');

$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.6, 20, 3.6);
$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.7, 20, 3.7);

if ($customers) {
    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->Ln(0.6);
    $this->fpdf->Cell(0, 0, 'Daftar Customer', 0, 0, 'C');

    $this->fpdf->SetFont('Times', 'B', 10);
    $this->fpdf->ln(1);
    $this->fpdf->Cell(1, 1, 'No', 1, 0, 'C');
    $this->fpdf->Cell(9.5, 1, 'Nama', 1, 0, 'C');
    $this->fpdf->Cell(8.5, 1, 'Kode Customer', 1, 0, 'C');


    $this->fpdf->Ln();

    $no = 1;
    $total = 0;
    foreach ($customers as $key => $value) {
        $this->fpdf->SetFont('Times', '', 11);
        $this->fpdf->Cell(1, 0.5, $no++, 1, 0, 'C');
        $this->fpdf->Cell(9.5, 0.5, $value['name'], 1, 0, 'L');
        $this->fpdf->Cell(8.5, 0.5, $value['cs_code'], 1, 0, 'L');
        $this->fpdf->Ln();
    }

} else {
    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->Ln(0.6);
    $this->fpdf->Cell(0, 0, "No data to show.", 0, 0, 'C');
}

$this->fpdf->Ln();

$this->fpdf->Output();