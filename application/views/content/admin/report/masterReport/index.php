<ul class="breadcrumb">
	<li>
		<a href="#">Laporan</a>
	</li>
	<li class="active">Data Master</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">
				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 style="font-weight: bold" class="panel-title">
							Data Master
						</h3>
					</div>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-3">
								<div class="panel panel-default">
									<div style="margin:auto;text-align:center;height:50px;margin-bottom:5%;margin-top:5%;"
										class="panel-body panel-body-image">
                                        <i style="font-size: 50px" class="fa fa-user"></i>
									</div>
									<div class="panel-footer text-muted">
										<a href="users" target="_blank" class="btn btn-success" style="width: 100%"> <i class="fa fa-print"></i> Laporan Pengguna</a>
									</div>
								</div>
							</div>
                            <div class="col-md-3">
								<div class="panel panel-default">
									<div style="margin:auto;text-align:center;height:50px;margin-bottom:5%;margin-top:5%;"
										class="panel-body panel-body-image">
                                        <i style="font-size: 50px" class="fa fa-users"></i>
									</div>
									<div class="panel-footer text-muted">
										<a href="customers" target="_blank" class="btn btn-success" style="width: 100%"> <i class="fa fa-print"></i> Laporan Customer</a>
									</div>
								</div>
							</div>
                            <div class="col-md-3">
								<div class="panel panel-default">
									<div style="margin:auto;text-align:center;height:50px;margin-bottom:5%;margin-top:5%;"
										class="panel-body panel-body-image">
                                        <i style="font-size: 50px" class="fa fa-truck"></i>
									</div>
									<div class="panel-footer text-muted">
										<a href="supliers" target="_blank" class="btn btn-success" style="width: 100%"> <i class="fa fa-print"></i> Laporan Suplier</a>
									</div>
								</div>
							</div>
                            <div class="col-md-3">
								<div class="panel panel-default">
									<div style="margin:auto;text-align:center;height:50px;margin-bottom:5%;margin-top:5%;"
										class="panel-body panel-body-image">
                                        <i style="font-size: 50px" class="fa fa-dropbox"></i>
									</div>
									<div class="panel-footer text-muted">
										<a href="items" target="_blank" class="btn btn-success" style="width: 100%"> <i class="fa fa-print"></i> Laporan Barang</a>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="masterReportjs">
	$(document).ready(function () {
		$("#masterReport_spin").hide();
		$("#masterReport_normal").show();
	});
</script>