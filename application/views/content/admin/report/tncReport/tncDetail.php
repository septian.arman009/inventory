<div class="panel panel-default tabs">
	<ul class="nav nav-tabs" role="tablist">
		<li id="inc-tab">
			<a onclick="tab_1()" href="#tab-first" role="tab" data-toggle="tab" aria-expanded="true">Barang Masuk</a>
		</li>
		<li id="otg-tab">
			<a onclick="tab_2()" href="#tab-second" role="tab" data-toggle="tab" aria-expanded="false">Barang Keluar</a>
		</li>
	</ul>
	<div class="panel-body tab-content">
		<div class="tab-pane" id="tab-first">
			<div class="panel-body">
			<a href="<?php echo base_url() ?>printAllIncoming/<?php echo $month.'/'.$year ?>" target="_blank" title="Cetak" class="btn btn-info btn-xs waves-effect"><i class="fa fa-print"> Cetak Laporan Barang Masuk Bulan <?php echo to_month($month) ?></i></a>
				<table class="table">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Transaksi</th>
							<th>Tanggal Barang Masuk</th>
							<th>Dibuat Oleh</th>
							<th>Update Terakhir</th>
                            <th>Jumlah Barang</th>
							<th>Daftar Barang</th>
							<th></th>
						</tr>
					</thead>

					<tbody>
						<?php $total = 0; $no = 1; foreach ($incoming as $key => $value) { ?>
						<tr>
							<td><?php echo $no++ ?></td>
							<td><?php echo $value['inc_code'] ?></td>
							<td><?php echo to_date($value['inc_date']) ?></td>
							<td><?php echo $this->main_model->gdo4p('users', 'name', 'user_id', $value['user_id']) ?>
							</td>
							<td><?php echo $this->main_model->gdo4p('users', 'name', 'user_id', $value['user_updated']) ?>
							</td>
                            <td>
                            <?php $list_incoming = unserialize($value['incoming']); 
                                echo array_sum(array_column($list_incoming, 'qty'))
                            ?>
                              Unit  
                            </td>
							<td>
								<div style="display: none;" id="detail<?php echo $value['incoming_id'] ?>" class="panel panel-default">
									<div class="panel-body">
										<div class="contact-info" id="detail">
											<p><small>Detail Barang</small><br>
                                                <?php foreach ($list_incoming as $key => $vli) { 
														$c_code = $this->main_model->gdo4p('items', 'c_code', 'i_code', $vli['i_code']);
														$category = $this->main_model->gdo4p('categories', 'name', 'c_code', $c_code);
                                                    echo "({$category}) {$vli['name']} ({$vli['i_code']}) : {$vli['qty']} unit <br>";
                                                 } ?>
                                            </p>
										</div>
									</div>
								</div>
                                <a id="btn1<?php echo $value['incoming_id'] ?>" style="font-decoration: none; cursor: pointer;" onclick="showDetail('#detail<?php echo $value['incoming_id'] ?>', 0, '<?php echo $value['incoming_id'] ?>')">Lihat Data</a>
                                <a id="btn2<?php echo $value['incoming_id'] ?>" style="display: none;" style="font-decoration: none; cursor: pointer;" onclick="showDetail('#detail<?php echo $value['incoming_id'] ?>', 1, '<?php echo $value['incoming_id'] ?>')">Tutup Data</a>
							</td>
							<td>
							<a href="<?php echo base_url() ?>printIncoming/<?php echo $value['incoming_id'] ?>" target="_blank" title="Cetak" class="btn btn-info btn-xs waves-effect"><i class="fa fa-print"></i></a>
							</td>
						</tr>
						<?php $total += array_sum(array_column($list_incoming, 'qty')); } ?>
                        <tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td><b>Total Barang Masuk</b></td>
                            <td><b><?php echo $total ?> Unit</b></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="tab-pane" id="tab-second">
        <div class="panel-body">
		<a href="<?php echo base_url() ?>printAllOutgoing/<?php echo $month.'/'.$year ?>" target="_blank" title="Cetak" class="btn btn-info btn-xs waves-effect"><i class="fa fa-print"> Cetak Laporan Barang Keluar Bulan <?php echo to_month($month) ?></i></a>
				<table class="table">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Transaksi</th>
							<th>Tanggal Barang Keluar</th>
							<th>Dibuat Oleh</th>
							<th>Update Terakhir</th>
                            <th>Jumlah Barang</th>
							<th>Daftar Barang</th>
							<th></th>
						</tr>
					</thead>

					<tbody>
						<?php $totalOtg = 0; $no = 1; foreach ($outgoing as $key => $vout) { ?>
						<tr>
							<td><?php echo $no++ ?></td>
							<td><?php echo $vout['otg_code'] ?></td>
							<td><?php echo to_date($vout['otg_date']) ?></td>
							<td><?php echo $this->main_model->gdo4p('users', 'name', 'user_id', $vout['user_id']) ?>
							</td>
							<td><?php echo $this->main_model->gdo4p('users', 'name', 'user_id', $vout['user_updated']) ?>
							</td>
                            <td>
                            <?php $list_outgoing = unserialize($vout['outgoing']); 
                                echo array_sum(array_column($list_outgoing, 'qty'))
                            ?>
                              Unit  
                            </td>
							<td>
								<div style="display: none;" id="detailOtg<?php echo $vout['outgoing_id'] ?>" class="panel panel-default">
									<div class="panel-body">
										<div class="contact-info" id="detail">
											<p><small>Detail Barang</small><br>
                                                <?php foreach ($list_outgoing as $key => $vlo) { 
													$c_code = $this->main_model->gdo4p('items', 'c_code', 'i_code', $vlo['i_code']);
													$category = $this->main_model->gdo4p('categories', 'name', 'c_code', $c_code);
                                                    echo "({$category}) {$vlo['name']} ({$vlo['i_code']}) : {$vlo['qty']} unit <br>";
                                                 } ?>
                                            </p>
										</div>
									</div>
								</div>
                                <a id="btn3<?php echo $vout['outgoing_id'] ?>" style="font-decoration: none; cursor: pointer;" onclick="showDetailOtg('#detailOtg<?php echo $vout['outgoing_id'] ?>', 0, '<?php echo $vout['outgoing_id'] ?>')">Lihat Data</a>
                                <a id="btn4<?php echo $vout['outgoing_id'] ?>" style="display: none;" style="font-decoration: none; cursor: pointer;" onclick="showDetailOtg('#detailOtg<?php echo $vout['outgoing_id'] ?>', 1, '<?php echo $vout['outgoing_id'] ?>')">Tutup Data</a>
							</td>
							<td>
								<a href="<?php echo base_url() ?>printOutgoing/<?php echo $vout['outgoing_id'] ?>" target="_blank" title="Cetak" class="btn btn-info btn-xs waves-effect"><i class="fa fa-print"></i></a>
							</td>
						</tr>
						<?php $totalOtg += array_sum(array_column($list_outgoing, 'qty')); } ?>
                        <tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td><b>Total Barang Keluar</b></td>
                            <td><b><?php echo $totalOtg ?> Unit</b></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
    function showDetail(id, status, tnc_id) {
        if(status == 0){
            $(id).toggle();
            $("#btn1"+tnc_id).hide();
            $("#btn2"+tnc_id).show();
        }else{
            $(id).toggle();
            $("#btn1"+tnc_id).show();
            $("#btn2"+tnc_id).hide();
        }
    }

    function showDetailOtg(id, status, tnc_id) {
        if(status == 0){
            $(id).toggle();
            $("#btn3"+tnc_id).hide();
            $("#btn4"+tnc_id).show();
        }else{
            $(id).toggle();
            $("#btn3"+tnc_id).show();
            $("#btn4"+tnc_id).hide();
        }
    }
    
	var tab_active = '<?php echo $tab_active ?>';

	if (tab_active == '#tab-first') {
		$('#tab-second').removeClass('active');
		$("#otg-tab").removeClass('active');
		$('#tab-first').addClass('active');
		$("#inc-tab").addClass('active');
		tab_1();
	} else {
		$('#tab-first').removeClass('active');
		$("#inc-tab").removeClass('active');
		$('#tab-second').addClass('active');
		$("#otg-tab").addClass('active');
		tab_2();
	}

	function tab_1() {
		document.cookie = "tab_active=#tab-first";
	}

	function tab_2() {
		document.cookie = "tab_active=#tab-second";
	}
</script>