<?php

$this->fpdf->FPDF_17('L', 'cm', 'A4');
$this->fpdf->AliasNbPages();
$this->fpdf->AddPage();

$this->fpdf->Image('assets/admin/img/logo.png', 1, 1, 2.5, 0, '', base_url('admin'));

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 24);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, 'CV. Hikari', 0, 0, 'C');

$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.8);
$this->fpdf->Cell(0, 0, 'Jl. Raya Bosih Central Ruko Selang BLok. A15-16, Cibitung - Bekasi 17520', 0, 0, 'C');
$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, 'Telp. 021 - 8839 2521 / 0821 2249 1680', 0, 0, 'C');

$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.6, 28.5, 3.6);
$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.7, 28.5, 3.7);

if ($incList) {
    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->Ln(0.6);
    $this->fpdf->Cell(0, 0, ' Kode Barang Masuk : '.$incData[0]['inc_code'], 0, 0, 'C');

    $this->fpdf->SetFont('Times', '', 11);
    $this->fpdf->Ln(1);
    $this->fpdf->Cell(3, 0, 'Dibuat Oleh', 0, 0, 'L');
    $this->fpdf->Cell(0.5, 0, ':', 0, 0, 'L');
    $this->fpdf->Cell(0.5, 0, $this->main_model->gdo4p('users', 'name', 'user_id', $incData[0]['user_id']), 0, 0, 'L');
    $this->fpdf->Ln(0.5);    
    $this->fpdf->Cell(3, 0, 'Update Terkahir', 0, 0, 'L');
    $this->fpdf->Cell(0.5, 0, ':', 0, 0, 'L');
    $this->fpdf->Cell(0.5, 0, $this->main_model->gdo4p('users', 'name', 'user_id', $incData[0]['user_updated']), 0, 0, 'L');
    $this->fpdf->Ln(0.5);    
    $this->fpdf->Cell(3, 0, 'Tanggal Masuk', 0, 0, 'L');
    $this->fpdf->Cell(0.5, 0, ':', 0, 0, 'L');
    $this->fpdf->Cell(0.5, 0, to_date($incData[0]['inc_date'])  , 0, 0, 'L');

    $this->fpdf->SetFont('Times', 'B', 10);
    $this->fpdf->ln(1);
    $this->fpdf->Cell(1, 1, 'No', 1, 0, 'C');
    $this->fpdf->Cell(4.5, 1, 'Kode Barang', 1, 0, 'C');
    $this->fpdf->Cell(4, 1, 'Kategori Barang', 1, 0, 'C');
    $this->fpdf->Cell(6.5, 1, 'Suplier', 1, 0, 'C');
    $this->fpdf->Cell(7.5, 1, 'Nama Barang', 1, 0, 'C');
    $this->fpdf->Cell(4, 1, 'Jumlah', 1, 0, 'C');


    $this->fpdf->Ln();

    $no = 1;
    $total = 0;
    foreach ($incList as $key => $value) {
        $c_code = $this->main_model->gdo4p('items', 'c_code', 'i_code', $value['i_code']);
		$category = $this->main_model->gdo4p('categories', 'name', 'c_code', $c_code);
        $this->fpdf->SetFont('Times', '', 11);
        $this->fpdf->Cell(1, 0.5, $no++, 1, 0, 'C');
        $this->fpdf->Cell(4.5, 0.5, $value['i_code'], 1, 0, 'L');
        $this->fpdf->Cell(4, 0.5, $category, 1, 0, 'L');
        $this->fpdf->Cell(6.5, 0.5, $this->main_model->gdo4p('supliers', 'name', 's_code', $value['s_code']), 1, 0, 'L');
        $this->fpdf->Cell(7.5, 0.5, $value['name'], 1, 0, 'L');
        $this->fpdf->Cell(4, 0.5, $value['qty']. ' Unit', 1, 0, 'L');
        $this->fpdf->Ln();
        $total += $value['qty'];
    }

    $this->fpdf->SetFont('Times', 'B', 11);
    $this->fpdf->Cell(1, 0.5, "",0, 0, 'C');
    $this->fpdf->Cell(4.5, 0.5, "", 0, 0, 'C');
    $this->fpdf->Cell(4, 0.5, "", 0, 0, 'C');
    $this->fpdf->Cell(6.5, 0.5, "", 0, 0, 'C');
    $this->fpdf->Cell(7.5, 0.5, "Total Barang", 1, 0, 'C');
    $this->fpdf->Cell(4, 0.5, $total.' Unit', 1, 0, 'L');

    $this->fpdf->Ln();
} else {
    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->Ln(0.6);
    $this->fpdf->Cell(0, 0, "No data to show.", 0, 0, 'C');
}

$this->fpdf->Ln();

$this->fpdf->Output();
