<ul class="breadcrumb">
	<li>
		<a href="#">Laporan</a>
	</li>
	<li class="active">Transaksi</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">
				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 style="font-weight: bold" class="panel-title">
							Barang Masuk & Keluar
						</h3>
					</div>

					<div class="panel-body">
					<div class="form-group">
						<label class="col-md-3 col-xs-12 control-label">Pilih Tahun</label>
							<div class="col-md-6 col-xs-12">
							<select onchange="filter($(this).val())" id="year" class="form-control">
								<?php for ($i = 2019; $i <= 2099; $i++) {?>
								<?php if ($i == $year) {?>
								<option selected value="<?php echo $i ?>">
									<?php echo $i ?>
								</option>
								<?php } else {?>
								<option value="<?php echo $i ?>">
									<?php echo $i ?>
								</option>
								<?php }?>
								<?php }?>
							</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div id="tncGraph"></div>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label class="col-md-8 control-label">Dari Tanggal</label>
							<div class="col-md-1">
								<select onchange="tncDetail()" id="start_date" class="form-control">
									<?php for ($i=1; $i <= 31; $i++) { ?>
										<option value="<?php echo $i ?>"><?php echo $i ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-2">
								<select onchange="tncDetail()" id="start_month" class="form-control">
									<?php
									foreach ($month as $key => $value) { ?>
										<option selected value="<?php echo $key ?>">
											<?php echo $value.' '.$year ?>
										</option>
									<?php }?>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-8 control-label">Sampai Dengan</label>
							<div class="col-md-1">
								<select onchange="tncDetail()" id="end_date" class="form-control">
									<?php for ($i=1; $i <= 31; $i++) { ?>
										<option value="<?php echo $i ?>"><?php echo $i ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-2">
								<select onchange="tncDetail()" id="end_month" class="form-control">
									<?php
									foreach ($month as $key => $value) { ?>
										<option selected value="<?php echo $key ?>">
											<?php echo $value.' '.$year ?>
										</option>
									<?php } ?>
								</select>
							</div>
						</div>
						<hr>

						<div id="tncDetail"></div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="tncReportjs">
	$(document).ready(function () {
		$("#tncReport_spin").hide();
		$("#tncReport_normal").show();
	});

	tncTable();

	$(function () {
		var chart;
		$(document).ready(function () {
			$.getJSON("report_controller/tncGraph/<?php echo $year ?>", function (json) {

				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'tncGraph',
						type: 'line'

					},
					title: {
						text: 'Statistic transaksi barang masuk & keluar tahun <?php echo $year ?>'

					},
					subtitle: {
						text: ''

					},
					credits: {
						enabled: false
					},
					xAxis: {
						categories: <?php echo json_encode($indoMonth) ?>
					},
					yAxis: {
						title: {
							text: 'Jumlah Barang'
						},
						plotLines: [{
							value: 0,
							width: 1,
							color: '#808080'
						}]
					},
					tooltip: {
						formatter: function () {
							return '<b>' + this.series.name + '</b><br/>' +
								this.x + ': ' + this.y;
						}
					},
					legend: {
						layout: 'vertical',
						align: 'right',
						verticalAlign: 'top',
						x: -10,
						y: 120,
						borderWidth: 0
					},
					series: json
				});
			});

		});

	});

	function filter(year) {
		var month = $("#month").val();
		loadView('report_controller/tncReport/' + month + '/' + year, '.content');
	}

	function tncDetail() {
		var start_date = $("#start_date").val();
		var start_month = $("#start_month").val();

		var end_date = $("#end_date").val();
		var end_month = $("#end_month").val();

		var year = $("#year").val();

		var start = year+'-'+start_month+'-'+start_date;
		var end = year+'-'+end_month+'-'+end_date;

		var url = 'report_controller/tncDetail/' + start + '/' + end + '/' + year;
		$('#tncDetail').load(url);
		var admin_url = 'report_controller/tncReport/' + start + '/' + end + '/' + year;
		document.cookie = "admin_url="+admin_url;
	}

	function tncTable() {
		var url = 'report_controller/tncDetail/<?php echo $start.'/'.$end.'/'.$year ?>';
		$('#tncDetail').load(url);
	}

	document.getElementById('tncReportjs').innerHTML = "";
</script>