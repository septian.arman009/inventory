<?php

$this->fpdf->FPDF_17('L', 'cm', 'A4');
$this->fpdf->AliasNbPages();
$this->fpdf->AddPage();

$this->fpdf->Image('assets/admin/img/logo.png', 1, 1, 2.5, 0, '', base_url('admin'));

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 24);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, 'CV. Hikari', 0, 0, 'C');

$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.8);
$this->fpdf->Cell(0, 0, 'Jl. Raya Bosih Central Ruko Selang BLok. A15-16, Cibitung - Bekasi 17520', 0, 0, 'C');
$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, 'Telp. 021 - 8839 2521 / 0821 2249 1680', 0, 0, 'C');

$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.6, 28.5, 3.6);
$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.7, 28.5, 3.7);

if ($otgData) {
    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->Ln(0.6);
    $this->fpdf->Cell(0, 0, ' Laporan Barang Keluar Bulan '.$month, 0, 0, 'C');

    $this->fpdf->SetFont('Times', 'B', 10);
    $this->fpdf->ln(1);
    $this->fpdf->Cell(1, 1, 'No', 1, 0, 'C');
    $this->fpdf->Cell(4, 1, 'Kode Transaksi', 1, 0, 'C');
    $this->fpdf->Cell(3, 1, 'Tgl Transaksi', 1, 0, 'C');
    $this->fpdf->Cell(9, 1, 'Daftar Barang', 1, 0, 'C');
    $this->fpdf->Cell(4, 1, 'Dibuat Oleh', 1, 0, 'C');
    $this->fpdf->Cell(4, 1, 'Diupdate Oleh', 1, 0, 'C');
    $this->fpdf->Cell(2.5, 1, 'Total Barang', 1, 0, 'C');


    $this->fpdf->Ln();

    $no = 1;
    $total = 0;
    foreach ($otgData as $key => $value) {
        $list = unserialize($value['outgoing']);
        $customer = $this->main_model->gdo4p('customers', 'name', 'cs_code', $list[0]['cs_code']);
        $qty = array_sum(array_column($list, 'qty'));
        $this->fpdf->SetFont('Times', '', 11);
        $this->fpdf->Cell(1, 0.5, $no++, 1, 0, 'C');
        $this->fpdf->Cell(4, 0.5, $value['otg_code'], 1, 0, 'L');
        $this->fpdf->Cell(3, 0.5, to_date($value['otg_date']), 1, 0, 'L');
        $this->fpdf->Cell(9, 0.5, "{$list[0]['name']} : {$list[0]['qty']} ({$customer})", 1, 0, 'L');
        $this->fpdf->Cell(4, 0.5, $this->main_model->gdo4p('users', 'name', 'user_id', $value['user_id']), 1, 0, 'L');
        $this->fpdf->Cell(4, 0.5, $this->main_model->gdo4p('users', 'name', 'user_id', $value['user_updated']), 1, 0, 'L');
        $this->fpdf->Cell(2.5, 0.5, $qty, 1, 0, 'L');
        $this->fpdf->Ln();
        
        $data = 1;
        foreach ($list as $key => $vl) {
            if($data == 2){
                $customer = $this->main_model->gdo4p('customers', 'name', 'cs_code', $vl['cs_code']);
                $this->fpdf->Cell(0.0001, 0.5, '', 1, 0, 'L');
                $this->fpdf->Cell(8, 0.5, '', 0, 0, 'C');
                $this->fpdf->Cell(9, 0.5, "{$vl['name']} : {$vl['qty']} ({$customer})", 1, 0, 'L');
                $this->fpdf->Cell(10.5, 0.5, '', 0, 0, 'C');
                $this->fpdf->Cell(0.0001, 0.5, '', 1, 0, 'L');
                $this->fpdf->Ln();
            }
            $data++;
        }
        $total += $qty;
    }

    $this->fpdf->Cell(8, 0, '', 1, 0, 'C');
    $this->fpdf->Cell(9, 0, '', 1, 0, 'L');
    $this->fpdf->Cell(10.5, 0, '', 1, 0, 'C');
    $this->fpdf->Ln();

    $this->fpdf->Cell(17, 0.5, '', 0, 0, 'L');
    $this->fpdf->Cell(8, 0.5, 'TOTAL', 1, 0, 'L');
    $this->fpdf->Cell(2.5, 0.5, $total, 1, 0, 'L');

    $this->fpdf->Ln();
} else {
    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->Ln(0.6);
    $this->fpdf->Cell(0, 0, "No data to show.", 0, 0, 'C');
}

$this->fpdf->Ln();

$this->fpdf->Output();
