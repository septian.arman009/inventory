<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-sign-out"></span> Log
				<strong>Out</strong> ?</div>
			<div class="mb-content">
				<p>Yakin ingin keluar ?</p>
				<p>Klik No untuk tetap bekerja, klik Yes untuk keluar aplikasi.</p>
			</div>
			<div class="mb-footer">
				<div class="pull-right">
					<a class="btn btn-success btn-lg" onclick="logout()">Yes</a>
					<button class="btn btn-default btn-lg mb-control-close">No</button>
				</div>
			</div>
		</div>
	</div>
</div>