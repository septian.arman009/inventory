<audio id="audio-alert" src="<?php echo base_url() ?>assets/admin/audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="<?php echo base_url() ?>assets/admin/audio/fail.mp3" preload="auto"></audio>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/jquery/jquery-ui.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/bootstrap/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/actions.js"></script>
<script src="<?php echo base_url() ?>assets/admin/myjs/main.js"></script>
<script src="<?php echo base_url() ?>assets/admin/myjs/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/myjs/jquery.maskMoney.js"></script>
<script src="<?php echo base_url() ?>assets/admin/myjs/bootstrap3-typeahead.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/HC/code/highcharts.js"></script>
<script src="<?php echo base_url() ?>assets/admin/sweetalert/sweetalert.min.js"></script>
</body>

<script id="footerjs">
	$(document).ready(function () {
		var admin_url = document.cookie.replace(/(?:(?:^|.*;\s*)admin_url\s*\=\s*([^;]*).*$)|^.*$/, "$1");

		if (admin_url) {
			var main_id = document.cookie.replace(/(?:(?:^|.*;\s*)main_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
			if (main_id) {
				var main_id = document.cookie.replace(/(?:(?:^|.*;\s*)main_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
				main_menu(main_id, admin_url);
			} else {
				var main_sub_id = document.cookie.replace(/(?:(?:^|.*;\s*)main_sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
				if (main_sub_id) {
					var sub_id = document.cookie.replace(/(?:(?:^|.*;\s*)sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
					sub_menu(main_sub_id, sub_id, admin_url);
				}

			}
		} else {
			main_menu('#home', 'main_controller/home/1');
		}
	});

	function spinner(div_spin, div_normal) {
		$(div_spin).show();
		$(div_normal).hide();
	}

	function logout() {
		document.cookie = "main_id=";
		document.cookie = "main_sub_id=";
		document.cookie = "sub_id=";
		document.cookie = "admin_url=";
		do_something("main_controller/logout");
		setTimeout(() => {
			location.reload();
		}, 500);
	}

	document.getElementById('footerjs').innerHTML = "";
</script>

<style>
	.no-sort::after {
		display: none !important;
	}

	.no-sort {
		pointer-events: none !important;
		cursor: default !important;
	}

	a {
		cursor: pointer;
	}
</style>

</html>