<!DOCTYPE html>
<html lang="en">

<head>
	<title>Apps - Inventory System</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="icon" href="<?php echo base_url() ?>assets/admin/img/logo.png" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url() ?>assets/admin/css/theme-white.css" />
	<link href="<?php echo base_url() ?>assets/admin/sweetalert/sweetalert.css" rel="stylesheet" />
</head>