<body>
	<!-- START PAGE CONTAINER -->
	<div class="page-container page-navigation-top-fixed">
		<!-- START PAGE SIDEBAR -->
		<div class="page-sidebar page-sidebar-fixed scroll">
			<!-- START X-NAVIGATION -->
			<ul class="x-navigation">
				<li class="xn-logo">
					<a style="font-size: 20px;">Inventory System</a>
					<a class="x-navigation-control"></a>
				</li>
				<li class="xn-profile">
					<a class="profile-mini">
						<img src="<?php echo base_url() ?>assets/admin/img/logo.png" alt="John Doe" />
					</a>
					<div class="profile">
						<div class="profile-image">
							<img src="<?php echo base_url() ?>assets/admin/img/logo.png" alt="John Doe" />
						</div>
						<div class="profile-data">
							<div class="profile-data-name">
								<?php echo $_SESSION['inv_in']['name'] ?>
							</div>
							<div class="profile-data-title">
								<?php echo $this->main_model->gdo4p('roles', 'display_name', 'name', $_SESSION['inv_in']['role']) ?>
							</div>
						</div>
						<div class="profile-controls">
							<a onclick="sub_menu('#master_data','#users','master_controller/form/users/<?php echo $_SESSION['inv_in']['id'] ?>/null','.content')"
								class="profile-control-left">
								<span class="fa fa-user"></span>
							</a>
							
							<?php if(role(['admin'], false)){ ?>
								<a onclick="sub_menu('#report','#tncReport','report_controller/tncReport/<?php echo $date_report[1].'/'.$date_report[0] ?>')" class="profile-control-right">
									<span class="fa fa-bar-chart-o"></span>
								</a>
							<?php } else { ?>
								<a onclick="sub_menu('#settings','#change_password','main_controller/change_password')" class="profile-control-right">
									<span class="fa fa-lock"></span>
								</a>
							<?php } ?>
						</div>
					</div>
				</li>

				<li class="xn-title">Versi 1.0.0</li>
				
				<li id="home">
					<a onmousedown="spinner('#home_spin', '#home_normal')"
						onclick="main_menu('#home','main_controller/home/1')">
						<span class="fa fa-home" id="home_normal"></span>
						<span class="fa fa-refresh fa-spin" style="display: none;" id="home_spin"></span>
						<span class="xn-text">Halaman Utama</span>
					</a>
				</li>
				<?php if(role(['admin'], false)){ ?>
				<li id="master_data" class="xn-openable">
					<a>
						<span class="fa fa-hdd-o"></span>
						<span class="xn-text">Data Master</span>
					</a>
					<ul>
						<li id="users">
							<a onmousedown="spinner('#users_spin','#users_normal')"
								onclick="sub_menu('#master_data','#users','master_controller/user')">
								<span class="fa fa-user" id="users_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="users_spin"></span>
								Pengguna
							</a>
						</li>
						<li id="customers">
							<a onmousedown="spinner('#customers_spin','#customers_normal')"
								onclick="sub_menu('#master_data','#customers','master_controller/customer')">
								<span class="fa fa-users" id="customers_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="customers_spin"></span>
								Customer
							</a>
						</li>
						<li id="supliers">
							<a onmousedown="spinner('#supliers_spin','#supliers_normal')"
								onclick="sub_menu('#master_data','#supliers','master_controller/suplier')">
								<span class="fa fa-truck" id="supliers_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="supliers_spin"></span>
								Suplier
							</a>
						</li>
						<li id="types">
							<a onmousedown="spinner('#types_spin','#types_normal')"
								onclick="sub_menu('#master_data','#types','master_controller/type')">
								<span class="fa fa-filter" id="types_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="types_spin"></span>
								Jenis Barang
							</a>
						</li>
						<li id="categories">
							<a onmousedown="spinner('#categories_spin','#categories_normal')"
								onclick="sub_menu('#master_data','#categories','master_controller/category')">
								<span class="fa fa-tags" id="categories_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="categories_spin"></span>
								Kategori Barang
							</a>
						</li>
						<li id="items">
							<a onmousedown="spinner('#items_spin','#items_normal')"
								onclick="sub_menu('#master_data','#items','master_controller/item')">
								<span class="fa fa-dropbox" id="items_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="items_spin"></span>
								Daftar Barang
							</a>
						</li>
					</ul>
				</li>

				<?php } ?>

				<li id="transaction" class="xn-openable">
					<a>
						<span class="fa fa-random"></span>
						<span class="xn-text">Transaksi</span>
					</a>
					<ul>
						<li id="incoming">
							<a onmousedown="spinner('#incoming_spin','#incoming_normal')"
								onclick="sub_menu('#transaction','#incoming','transaction_controller/incoming/null')">
								<span class="fa fa fa-arrow-circle-down" id="incoming_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="incoming_spin"></span>
								Barang Masuk
							</a>
						</li>
						<li id="outgoing">
							<a onmousedown="spinner('#outgoing_spin','#outgoing_normal')"
								onclick="sub_menu('#transaction','#outgoing','transaction_controller/outgoing/null')">
								<span class="fa fa-arrow-circle-up" id="outgoing_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="outgoing_spin"></span>
								Barang Keluar
							</a>
						</li>
					</ul>
				</li>
				
				<?php if(role(['admin'], false)){ ?>
				<li id="report" class="xn-openable">
					<a>
						<span class="fa fa-book"></span>
						<span class="xn-text">Laporan</span>
					</a>
					<ul>
						<li id="tncReport">
							<a onmousedown="spinner('#tncReport_spin','#tncReport_normal')"
								onclick="sub_menu('#report','#tncReport','report_controller/tncReport/0000-00-00/0000-00-00/<?php echo $date_report[0] ?>')">
								<span class="fa fa-bar-chart-o" id="tncReport_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="tncReport_spin"></span>
								Transaksi
							</a>
						</li>
						<li id="masterReport">
							<a onmousedown="spinner('#masterReport_spin','#masterReport_normal')"
								onclick="sub_menu('#report','#masterReport','report_controller/masterReport')">
								<span class="fa fa-file" id="masterReport_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="masterReport_spin"></span>
								Data Master
							</a>
						</li>
					</ul>
				</li>
				<?php }?>

				<li id="settings" class="xn-openable">
					<a>
						<span class="fa fa-gear"></span>
						<span class="xn-text">Pengaturan</span>
					</a>
					<ul>
					<?php if(role(['admin'], false)){ ?>
						<li id="email_sender">
							<a onmousedown="spinner('#email_sender_spin','#email_sender_normal')"
								onclick="sub_menu('#settings','#email_sender','main_controller/email_sender')">
								<span class="fa fa-envelope" id="email_sender_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;"
									id="email_sender_spin"></span>
								Email Pengirim Pesan
							</a>
						</li>
					<?php } ?>
						<li id="change_password">
							<a onmousedown="spinner('#change_password_spin','#change_password_normal')"
								onclick="sub_menu('#settings','#change_password','main_controller/change_password')">
								<span class="fa fa-lock" id="change_password_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;"
									id="change_password_spin"></span>
								Ganti Password
							</a>
						</li>
					</ul>
				</li>
				<!-- END X-NAVIGATION -->
		</div>
		<!-- END PAGE SIDEBAR -->