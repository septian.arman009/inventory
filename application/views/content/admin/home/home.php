<ul class="breadcrumb">
	<li>
		<a href="#">Halaman Utama</a>
	</li>
</ul>

<div class="page-content-wrap">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong>Log</strong> Aktifitas Pengguna</h3>
					</div>
					<div class="panel-body">
						<div class="col-md-12">
							<a onclick="prev()" class="btn btn-default">
								<i class="fa fa-arrow-left"></i> Sebelumnya</a>
							<?php echo ' Halaman : ' . $page . ' Dari ' . $all ?>
							<a onclick="next()" class="btn btn-default">Selanjutnya
								<i class="fa fa-arrow-right"></i>
							</a>

						</div>
					</div>
					<div class="panel-body">
						<?php if($logs){ ?>
						<div class="messages" style="height:60vh;overflow:auto;overflow-y:scroll;">
							<?php foreach ($logs as $key => $value) {?>
							<div class="item item-visible">
								<div class="text">
									<div class="heading">
										<a>
											(User ID :
											<?php echo $value['user_id'] ?>)
											<?php echo $value['name'] ?>
											<span style="color: black" class="date">
												<?php echo to_date_time($value['created_at']) ?>
											</span>
										</a>
									</div>
									<?php echo $value['log'] ?>
								</div>
							</div>
							<?php }?>
						</div>
						<?php }else{?>
						<div class="messages" style="height:70vh;overflow:auto;overflow-y:scroll;">
							No Data
						</div>
						<?php }?>
					</div>
				</div>

			</div>
		</div>
	</div>

	<script id="homejs">
		$(document).ready(function () {
			$("#home_spin").hide();
			$("#home_normal").show();
		});

		function next() {
			var page = <?php echo $page + 1 ?>;
			if (page >= 0) {
				loadView('main_controller/home/' + page, '.content');
			}
		}

		function prev() {
			var page = <?php echo $page - 1 ?>;
			if (page > 0) {
				loadView('main_controller/home/' + page, '.content');
			}
		}

		document.getElementById('homejs').innerHTML = "";
	</script>