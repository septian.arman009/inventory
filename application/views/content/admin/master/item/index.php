<ul class="breadcrumb">
	<li>
		<a href="#">Data Master</a>
	</li>
	<li class="active">Daftar Barang</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar Barang</h3>
				</div>
				<div class="panel-body">
					<button onclick="loadView('master_controller/form/items/null/null', '.content')" class="btn btn-default mb-control"
					 data-box="#message-box-sound-2">Tambah Barang</button>
					<br>
					<br>
					<table id="item-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">No</th>
								<th id="th">Kode Barang</th>
								<th id="th">Kategori Barang</th>
								<th id="th">Nama</th>
                                <th id="th">Quantity</th>
								<th id="th" class="no-sort" width="10%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">no</th>
								<th class="footer">Kode</th>
								<th class="footer">Kategori</th>
								<th class="footer">Nama</th>
                                <th class="footer">Quantity</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->
		</div>
	</div>
</div>

<script id="itemjs">
	$(document).ready(function () {
		$("#items_spin").hide();
		$("#items_normal").show();

		$('#item-table tfoot th').each(function () {
			var title = $(this).text();
			var inp = '<input type="text" class="form-control footer-s" placeholder="' + title + '" id="' + title + '" />';
			$(this).html(inp);
		});

		var table = $('#item-table').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": 'master_controller/item_table',
				"type": "POST"
			}
		});

		table.columns().every(function () {
			var that = this;
			$('input', this.footer()).on('keyup change', function () {
				if (that.search() !== this.value) {
					that.search(this.value).draw();
				}
			});
		});

		$("#no").hide();
	});

	function destroy(id) {
		swal({
				title: "Apakah anda yakin ?",
				text: "Setelah di hapus data tidak dapat dikembalikan lagi.",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Ya, Hapus Sekarang!",
				closeOnConfirm: false
			},
			function () {
				$(".confirm").attr('disabled', 'disabled');
				$(".cancel").attr('disabled', 'disabled');

				var data = {
					id: id
				}
				postData('main_controller/destroy/items/item_id', data, function (err, response) {
					if (response) {
						if (response.status == 'success') {
							loadView('master_controller/item', '.content');
							swal("Sukses", "Berhasil menghapus data barang.", "success");
							$(".confirm").removeAttr('disabled');
							$(".cancel").removeAttr('disabled');
						} else {
							swal("Terjadi Kesalahan", "Gagal menghapus data barang, barang masih digunakan dalam transaksi.", "error");
							$(".confirm").removeAttr('disabled');
							$(".cancel").removeAttr('disabled');
						}
					} else {
						console.log('ini error : ', err);
					}
				});
			}
		);
	}

	function edit(id) {
		loadView('master_controller/form/items/' + id + '/null', '.content');
	}

	document.getElementById('itemjs').innerHTML = "";
</script>

<style>
	#item-table_filter {
		display: none;
	}
</style>