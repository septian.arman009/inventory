<ul class="breadcrumb">
	<li>
		<a href="#">Data Master </a>
	</li>
	<li>Daftar Barang</li>
	<?php if ($id == 'null') {?>
		<li class="active">Tambah Barang</li>
	<?php }else{?>
		<li class="active">Ubah Barang</li>
	<?php }?>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">
				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<?php if ($id == 'null') {?>
						<h3 class="panel-title">
							<strong>Tambah</strong> Barang</h3>
						<?php }else{?>
						<h3 class="panel-title">
							<strong>Ubah</strong> Barang</h3>
						<?php }?>
					</div>

					<a onclick="loadView('master_controller/item', '.content')" class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
					</a>

					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">

								<div class="form-group">
									<label class="col-md-3 control-label">Kode Barang</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-key"></span>
											</span>
											<input style="color: black" disabled value="<?php if($id == 'null'){echo '...'.$i_code;} ?>" id="i_code" type="text" class="form-control">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label">Nama Barang</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-dropbox"></span>
											</span>
											<input onkeyup="generate_code($(this).val())" id="name" type="text" class="form-control">
										</div>
									</div>
								</div>

                                <div class="form-group">
									<label class="col-md-3 control-label">Jenis Barang</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-filter"></span>
											</span>
											<select onchange="generate_code()" class="form-control" id="t_code">
												<option value="">- Pilih Jenis Barang -</option>
												<?php foreach ($t_code as $key => $value) { ?>
                                                    <option value="<?php echo $value['t_code'] ?>">
                                                        <?php echo $value['name'] ?>
                                                    </option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label">Kategori</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-tags"></span>
											</span>
											<select onchange="form_check()" class="form-control" id="c_code">
												<option value="">- Pilih Kategori -</option>
												<?php foreach ($c_code as $key => $value) { ?>
                                                    <option value="<?php echo $value['c_code'] ?>">
                                                        <?php echo $value['name'] ?>
                                                    </option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label">Suplier</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-truck"></span>
											</span>
											<select onchange="form_check()" class="form-control" id="s_code">
												<option value="">- Pilih Suplier -</option>
												<?php foreach ($s_code as $key => $value) { ?>
                                                    <option value="<?php echo $value['s_code'] ?>">
                                                        <?php echo $value['name'] ?>
                                                    </option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label">Quantity</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-unsorted"></span>
											</span>
											<input onkeyup="form_check()" id="qty" type="number" class="form-control">											
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="panel-footer">
						<a id="clear" onclick="reset()" class="btn btn-default">Bersihkan</a>
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right">Simpan</a>
						<a style="display:none" id="loading" class="btn btn-primary pull-right">Mohon Tunggu ..</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="formitemjs">
	var id = '<?php echo $id ?>';

	get_data(id);

	function reset() {
		$("#i_code").val('<?php echo $i_code ?>');
		$("#name").val('');
		$("#t_code").val('');
		$("#c_code").val('');
		$("#s_code").val('');
		$("#qty").val('');
		$("#save").attr('disabled', 'disabled');
	}

	function form_check() {
		var name = $("#name").val();
		var t_code = $("#t_code").val();
		var c_code = $("#c_code").val();
		var s_code = $("#s_code").val();
		var qty = $("#qty").val();
		if (name != '' && t_code != '' && c_code != '' && s_code != '' && qty != '') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			id: id,
			table: 'items',
			i_code: $('#i_code').val(),
			name: $('#name').val(),
			t_code: $('#t_code').val(),
			c_code: $('#c_code').val(),
			s_code: $('#s_code').val(),
			qty: $('#qty').val()
		}

		postData('master_controller/item_action', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					swal("Sukses", "Data jenis barang berhasil disimpan.", "success");
					if(id == 'null'){
						loadView('master_controller/form/items/' + id + '/null', '.content');
					}
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Terjadi Kesalahan", "Gagal simpan data jenis barang.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function get_data(id) {
		if (id != 'null') {
			data = {
				id: id,
				column: 'item_id',
				table: 'items'
			}

			postData('main_controller/get_data', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#i_code").val(response.data[0].i_code);
						$("#name").val(response.data[0].name);
						$("#t_code").val(response.data[0].t_code);
						$("#c_code").val(response.data[0].c_code);
						$("#s_code").val(response.data[0].s_code);
						$("#qty").val(response.data[0].qty);
						$("#clear").attr('disabled', 'disabled');
						setTimeout(() => {
							form_check();
						}, 100);
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}
	}

	function generate_code() {
		if(id == 'null'){
			var t_code = $("#t_code").val().replaceAll('/','');
			var name = $("#name").val();
			var name_code = '';
			if(name != ''){
				var array_name = name.split(' ');
				for(var i=0; i < array_name.length; i++){
					if(name_code == ''){
						name_code = array_name[i].slice(0,1).toUpperCase();
					}else{
						name_code = name_code+''+array_name[i].slice(0,1).toUpperCase();
					}
				}
				var i_code = t_code +'-'+ name_code +''+ '<?php echo $i_code ?>';
			}else{
				var i_code = '<?php echo "...".$i_code ?>';
			}
			
			$("#i_code").val(i_code);
			form_check();
		}
	}

	document.getElementById('formitemjs').innerHTML = "";
</script>