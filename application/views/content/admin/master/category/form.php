<ul class="breadcrumb">
	<li>
		<a href="#">Data Master </a>
	</li>
	<li>Jenis Barang</li>
	<?php if ($id == 'null') {?>
		<li class="active">Tambah Kategori Barang</li>
	<?php }else{?>
		<li class="active">Ubah Kategori Barang</li>
	<?php }?>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">
				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<?php if ($id == 'null') {?>
						<h3 class="panel-title">
							<strong>Tambah</strong> Kategori Barang</h3>
						<?php }else{?>
						<h3 class="panel-title">
							<strong>Ubah</strong> Kategori Barang</h3>
						<?php }?>
					</div>

					<a onclick="loadView('master_controller/category', '.content')" class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
					</a>

					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">

								<div class="form-group">
									<label class="col-md-3 control-label">Kode Kategori Barang</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-key"></span>
											</span>
											<input style="color: black" disabled id="c_code" type="text" value="<?php echo $c_code ?>" class="form-control">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label">Kategori</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-tags"></span>
											</span>
											<input onkeyup="form_check()" id="name" type="text" class="form-control">
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="panel-footer">
						<a id="clear" onclick="reset()" class="btn btn-default">Bersihkan</a>
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right">Simpan</a>
						<a style="display:none" id="loading" class="btn btn-primary pull-right">Mohon Tunggu ..</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="formcategoryjs">
	var id = '<?php echo $id ?>';

	get_data(id);

	function reset() {
		$("#name").val('');
		form_check();
	}

	function form_check() {
		var name = $("#name").val();
		if (name != '') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			id: id,
			table: 'categories',
			c_code: $('#c_code').val(),
			name: $('#name').val()
		}

		postData('master_controller/category_action', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					swal("Sukses", "Data kategori barang berhasil disimpan.", "success");
					if(id == 'null'){
						loadView('master_controller/form/categories/' + id + '/null', '.content');
					}
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Terjadi Kesalahan", "Gagal simpan data kategori barang.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function get_data(id) {
		if (id != 'null') {
			data = {
				id: id,
				column: 'category_id',
				table: 'categories'
			}

			postData('main_controller/get_data', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#c_code").val(response.data[0].c_code);
						$("#name").val(response.data[0].name);
						$("#clear").attr('disabled', 'disabled');
						setTimeout(() => {
							form_check();
						}, 100);
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}
	}

	document.getElementById('formcategoryjs').innerHTML = "";
</script>