<ul class="breadcrumb">
	<li>
		<a href="#">Data Master </a>
	</li>
	<li>Pengguna</li>
	<?php if ($id == 'null') {?>
	<li class="active">Tambah Pengguna</li>
	<?php }else{?>
	<li class="active">Ubah Pengguna</li>
	<?php }?>

</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">
				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<?php if ($id == 'null') {?>
						<h3 class="panel-title">
							<strong>Tambah</strong> Pengguna</h3>
						<?php }else{?>
						<h3 class="panel-title">
							<strong>Ubah</strong> Pengguna</h3>
						<?php }?>
					</div>
					<?php if(role(['admin'], false)){ ?>
					<a onclick="sub_menu('#master_data','#users','master_controller/user')" class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
					</a>
					<?php }else{ ?>
					<a onclick="main_menu('#home','main_controller/home/1')" class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
					</a>
					<?php } ?>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label">Nama</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-user"></span>
											</span>
											<input onkeyup="form_check()" id="name" type="text" class="form-control">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Email</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-envelope-o"></span>
											</span>
											<input autocomplete="new-email" onkeyup="email_check($(this).val())" id="email" type="email"
												class="form-control">
										</div>
										<span class="help-block" style="display:none;color:red"
											id="invalid_email">Masukan format email yang benar.</span>
										<span class="help-block" style="display:none;color:red" id="registered">Email
											sudah terdaftar.</span>
									</div>
								</div>

								<?php if($id == 'null'){ ?>
								<div class="form-group">
									<label class="col-md-3 control-label">Password</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-lock"></span>
											</span>
											<input autocomplete="new-password" onkeyup="check_password()" id="password" type="password"
												class="form-control">
										</div>
										<p style="display:none;color:red" id="passless">Min 8 karakter.</p>
										<p style="display:none;color:red" id="notmatch">Password tidak sama.</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Konfirmasi</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-lock"></span>
											</span>
											<input onkeyup="check_password()" id="confirm" type="password"
												class="form-control">
										</div>
										<div style="display:none;color:red" id="conless">Min 8 karakter.</div>
										<div style="display:none;color:red" id="unmatch">Password tidak sama.</div>
									</div>
								</div>

								<?php } ?>

								<div class="form-group">
									<label class="col-md-3 control-label">Alamat</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-home"></span>
											</span>
											<textarea onkeyup="form_check()" id="address"
												class="form-control"></textarea>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Telpon</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-phone"></span>
											</span>
											<input onkeyup="form_check()" id="phone" type="number" class="form-control">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Role</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-key"></span>
											</span>
											<select onchange="form_check()" class="form-control" id="role_id">
												<?php foreach ($roles as $key => $value) { ?>
												<?php if(role(['admin'], false)){ ?>
												<option value="<?php echo $value['role_id'] ?>">
													<?php echo $value['display_name'] ?>
												</option>
												<?php } else {?>
												<?php if($value['role_id'] != 1 ) { ?>
												<option value="<?php echo $value['role_id'] ?>">
													<?php echo $value['display_name'] ?>
												</option>
												<?php } ?>
												<?php } ?>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<a id="clear" onclick="reset()" class="btn btn-default">Bersihkan</a>
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right">Simpan</a>
						<a style="display:none" id="loading" class="btn btn-primary pull-right">Mohon Tunggu ..</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="formuserjs">
	var id = '<?php echo $id ?>';

	get_data(id);

	function reset() {
		$("#e_id").val('');
		$("#name").val('');
		$("#email").val('');
		$("#password").val('');
		$("#confirm").val('');
		$("#address").val('');
		$("#phone").val('');
		this.v_email = '';
		form_check();
	}

	function form_check() {
		var name = $("#name").val();
		var address = $("#address").val();
		var phone = $("#phone").val();
		if (name != '' && address != '' && phone != '' && this.v_email == 'true' && this.passCheck == 'true') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function email_check(email) {
		if (email != '') {
			if (ValidateEmail(email, "#save", "#invalid_email")) {
				var data = {
					id: id,
					value: $("#email").val(),
					column: 'email',
					column_id: 'user_id',
					table: 'users'
				}

				postData('main_controller/single_data_check', data, function (err, response) {
					if (response) {
						var status = response.status;
						if (status == 'success') {
							$("#invalid_email").hide();
							$("#registered").hide();
							this.v_email = 'true';
							setTimeout(() => {
								form_check();
							}, 100);
						} else {
							$("#invalid_email").hide();
							$("#registered").show();
							this.v_email = 'false';
							$("#save").attr('disabled', 'disabled');
						}
					} else {
						console.log('ini error : ', err);
					}
				});
			}
		} else {
			$("#invalid_email").hide();
			$("#registered").hide();
			this.v_email = '';
			$("#save").attr('disabled', 'disabled');
		}
	}

	function check_password() {
		var password = $("#password").val();
		var confirm = $("#confirm").val();

		var passlen = password.length;
		var conlen = confirm.length;

		if (passlen > 0 && passlen < 8) {
			$("#passless").show();
		} else {
			$("#passless").hide();
		}

		if (conlen > 0 && conlen < 8) {
			$("#conless").show();
			$("#unmatch").hide();
		} else {
			$("#conless").hide();
			$("#unmatch").hide();
			if (password.length >= 8 && confirm.length >= 8) {
				if (password == confirm) {
					this.passCheck = 'true';
					$("#unmatch").hide();
					$("#save").removeAttr('disabled');
					setTimeout(() => {
						form_check();
					}, 100);
				} else {
					this.passCheck = 'false';
					$("#unmatch").show();
					setTimeout(() => {
						form_check();
					}, 100);
				}
			} else {
				this.passCheck = 'false';
				setTimeout(() => {
						form_check();
					}, 100);
			}
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			id: id,
			table: 'users',
			name: $('#name').val(),
			email: $('#email').val(),
			password: $('#password').val(),
			address: $('#address').val(),
			phone: $('#phone').val(),
			role_id: $('#role_id').val()
		}

		postData('master_controller/user_action', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					swal("Sukses", "Data pengguna berhasil disimpan.", "success");
					if (id == 'null') {
						reset();
					}
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Terjadi Kesalahan", "Gagal simpan data pengguna.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function get_data(id) {
		if (id != 'null') {
			data = {
				id: id,
				column: 'user_id',
				table: 'users'
			}

			postData('main_controller/get_data', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#name").val(response.data[0].name);
						$("#email").val(response.data[0].email);
						$("#address").val(response.data[0].address);
						$("#phone").val(response.data[0].phone);
						$("#role").val(response.data[0].role);
						$("#clear").attr('disabled', 'disabled');
						this.v_email = 'true';
						this.passCheck = 'true';
						setTimeout(() => {
							form_check();
						}, 100);
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}
	}

	document.getElementById('formuserjs').innerHTML = "";
</script>