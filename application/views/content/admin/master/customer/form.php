<ul class="breadcrumb">
	<li>
		<a href="#">Data Master </a>
	</li>
	<li>Customer</li>
	<?php if ($id == 'null') {?>
		<li class="active">Tambah Customer</li>
	<?php }else{?>
		<li class="active">Ubah Customer</li>
	<?php }?>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">
				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<?php if ($id == 'null') {?>
						<h3 class="panel-title">
							<strong>Tambah</strong> Customer</h3>
						<?php }else{?>
						<h3 class="panel-title">
							<strong>Ubah</strong> Customer</h3>
						<?php }?>
					</div>

					<a onclick="loadView('master_controller/customer', '.content')" class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
					</a>

					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">

								<div class="form-group">
									<label class="col-md-3 control-label">Kode Customer</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-key"></span>
											</span>
											<input style="color: black" disabled id="cs_code" value="<?php if($id == 'null'){echo '...'.$cs_code;} ?>" type="text" class="form-control">
										</div>
										<span class="help-block" style="display:none;color:red" id="registered">Kode Jenis Barang sudah terdaftar.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label">Nama Customer</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-truck"></span>
											</span>
											<input onkeyup="generate_code()" id="name" type="text" class="form-control">
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="panel-footer">
						<a id="clear" onclick="reset()" class="btn btn-default">Bersihkan</a>
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right">Simpan</a>
						<a style="display:none" id="loading" class="btn btn-primary pull-right">Mohon Tunggu ..</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="formsuplierjs">
	var id = '<?php echo $id ?>';

	get_data(id);

	function reset() {
		$("#cs_code").val('');
		$("#name").val('');
		$("#save").attr('disabled', 'disabled');
	}

	function form_check(name) {
		if (name != '') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function code_check(cs_code, name) {
		if (cs_code != '') {
			var data = {
				id: id,
				value: cs_code,
				column: 'cs_code',
				column_id: 'customer_id',
				table: 'customers'
			}

			postData('main_controller/single_data_check', data, function (err, response) {
				if (response) {
					var status = response.status;
					if (status == 'success') {
						$("#registered").hide();
						form_check(name);
					} else {
						$("#registered").show();
						$("#save").attr('disabled', 'disabled');
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}else{
			$("#registered").hide();
			form_check(name);
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			id: id,
			table: 'customers',
			cs_code: $('#cs_code').val(),
			name: $('#name').val()
		}

		postData('master_controller/customer_action', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					swal("Sukses", "Data customer berhasil disimpan.", "success");
					if(id == 'null'){
		                loadView('master_controller/form/customers/' + id + '/null', '.content');
					}
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Terjadi Kesalahan", "Gagal simpan data customer.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function get_data(id) {
		if (id != 'null') {
			data = {
				id: id,
				column: 'customer_id',
				table: 'customers'
			}

			postData('main_controller/get_data', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#cs_code").val(response.data[0].cs_code);
						$("#name").val(response.data[0].name);
						$("#clear").attr('disabled', 'disabled');
						setTimeout(() => {
							form_check();
						}, 100);
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}
	}

	function generate_code() {
		if(id == 'null'){
			var name = $("#name").val();
			var name_code = '';
			if(name != ''){
				var array_name = name.split(' ');
				for(var i=0; i < array_name.length; i++){
					if(name_code == ''){
						name_code = array_name[i].slice(0,1).toUpperCase();
					}else{
						name_code = name_code+''+array_name[i].slice(0,1).toUpperCase();
					}
				}
				var cs_code = name_code+''+ '<?php echo $cs_code ?>';
			}else{
				var cs_code = '<?php echo "...".$cs_code ?>';
			}
			
			$("#cs_code").val(cs_code);
			code_check(cs_code, name);
		}
	}

	document.getElementById('formsuplierjs').innerHTML = "";
</script>