<ul class="breadcrumb">
	<li>
		<a href="#">Data Master </a>
	</li>
	<li>Jenis Barang</li>
	<?php if ($id == 'null') {?>
		<li class="active">Tambah Jenis Barang</li>
	<?php }else{?>
		<li class="active">Ubah Jenis Barang</li>
	<?php }?>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">
				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<?php if ($id == 'null') {?>
						<h3 class="panel-title">
							<strong>Tambah</strong> Jenis Barang</h3>
						<?php }else{?>
						<h3 class="panel-title">
							<strong>Ubah</strong> Jenis Barang</h3>
						<?php }?>
					</div>

					<a onclick="loadView('master_controller/type', '.content')" class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
					</a>

					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">

								<div class="form-group">
									<label class="col-md-3 control-label">Kode Jenis Barang</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-key"></span>
											</span>
											<input style="color: black" disabled id="t_code" type="text" class="form-control">
										</div>
										<span class="help-block" style="display:none;color:red" id="registered">Kode Jenis Barang sudah terdaftar.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label">Jenis Barang</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-filter"></span>
											</span>
											<input onkeyup="generate_code()" id="name" type="text" class="form-control">
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="panel-footer">
						<a id="clear" onclick="reset()" class="btn btn-default">Bersihkan</a>
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right">Simpan</a>
						<a style="display:none" id="loading" class="btn btn-primary pull-right">Mohon Tunggu ..</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="formtypejs">
	var id = '<?php echo $id ?>';

	get_data(id);

	function reset() {
		$("#t_code").val('');
		$("#name").val('');
		$("#save").attr('disabled', 'disabled');
	}

	function form_check(name) {
		if (name != '') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function code_check(t_code, name) {
		if (t_code != '') {
			var data = {
				id: id,
				value: t_code,
				column: 't_code',
				column_id: 'type_id',
				table: 'types'
			}

			postData('main_controller/single_data_check', data, function (err, response) {
				if (response) {
					var status = response.status;
					if (status == 'success') {
						$("#registered").hide();
						form_check(name);
					} else {
						$("#registered").show();
						$("#save").attr('disabled', 'disabled');
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}else{
			$("#registered").hide();
			form_check(name);
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			id: id,
			table: 'types',
			t_code: $('#t_code').val(),
			name: $('#name').val()
		}

		postData('master_controller/type_action', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					swal("Sukses", "Data jenis barang berhasil disimpan.", "success");
					if(id == 'null'){
						reset();
					}
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Terjadi Kesalahan", "Gagal simpan data jenis barang.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function get_data(id) {
		if (id != 'null') {
			data = {
				id: id,
				column: 'type_id',
				table: 'types'
			}

			postData('main_controller/get_data', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#t_code").val(response.data[0].t_code);
						$("#name").val(response.data[0].name);
						$("#clear").attr('disabled', 'disabled');
						this.v_code = 'true';
						setTimeout(() => {
							form_check();
						}, 100);
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}
	}

	function generate_code() {
		if(id == 'null'){
			var name = $("#name").val();
			var name_code = '';
			if(name != ''){
				var array_name = name.split(' ');
				for(var i=0; i < array_name.length; i++){
					if(name_code == ''){
						name_code = array_name[i].slice(0,1).toUpperCase();
					}else{
						name_code = name_code+'/'+array_name[i].slice(0,1).toUpperCase();
					}
				}
			}
			
			var t_code = name_code;
			$("#t_code").val(t_code);
			code_check(t_code, name);
		}
	}

	document.getElementById('formtypejs').innerHTML = "";
</script>