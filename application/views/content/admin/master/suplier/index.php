<ul class="breadcrumb">
	<li>
		<a href="#">Data Master</a>
	</li>
	<li class="active">Suplier</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar Suplier</h3>
				</div>
				<div class="panel-body">
					<button onclick="loadView('master_controller/form/supliers/null/null', '.content')" class="btn btn-default mb-control"
					 data-box="#message-box-sound-2">Tambah Suplier</button>
					<br>
					<br>
					<table id="suplier-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">No</th>
								<th id="th">Kode Suplier</th>
								<th id="th">Nama</th>
								<th id="th" class="no-sort" width="10%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">no</th>
								<th class="footer">Kode</th>
								<th class="footer">Nama</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->
		</div>
	</div>
</div>

<script id="suplierjs">
	$(document).ready(function () {
		$("#supliers_spin").hide();
		$("#supliers_normal").show();

		$('#suplier-table tfoot th').each(function () {
			var title = $(this).text();
			var inp = '<input type="text" class="form-control footer-s" placeholder="' + title + '" id="' + title + '" />';
			$(this).html(inp);
		});

		var table = $('#suplier-table').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": 'master_controller/suplier_table',
				"type": "POST"
			}
		});

		table.columns().every(function () {
			var that = this;
			$('input', this.footer()).on('keyup change', function () {
				if (that.search() !== this.value) {
					that.search(this.value).draw();
				}
			});
		});

		$("#no").hide();
	});

	function destroy(id) {
		swal({
				title: "Apakah anda yakin ?",
				text: "Setelah di hapus data tidak dapat dikembalikan lagi.",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Ya, Hapus Sekarang!",
				closeOnConfirm: false
			},
			function () {
				$(".confirm").attr('disabled', 'disabled');
				$(".cancel").attr('disabled', 'disabled');

				var data = {
					id: id
				}
				postData('main_controller/destroy/supliers/suplier_id', data, function (err, response) {
					if (response) {
						if (response.status == 'success') {
							loadView('master_controller/suplier', '.content');
							swal("Sukses", "Berhasil menghapus data suplier.", "success");
							$(".confirm").removeAttr('disabled');
							$(".cancel").removeAttr('disabled');
						} else {
							swal("Terjadi Kesalahan", "Suplier masih digunakan pada tabel barang.", "error");
							$(".confirm").removeAttr('disabled');
							$(".cancel").removeAttr('disabled');
						}
					} else {
						console.log('ini error : ', err);
					}
				});
			}
		);
	}

	function edit(id) {
		loadView('master_controller/form/supliers/' + id + '/null', '.content');
	}

	document.getElementById('suplierjs').innerHTML = "";
</script>

<style>
	#suplier-table_filter {
		display: none;
	}
</style>