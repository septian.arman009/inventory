<div class="panel-body">
	<div class="alert alert-danger" role="alert">
		<?php echo $button_back ?>
		<strong>Error !</strong>
		<?php echo $message ?>
	</div>
</div>

<script>
	$(document).ready(function () {
		var to_show = '<?php echo $to_show ?>';
		var to_hide = '<?php echo $to_hide ?>';
		$(to_show).show();
		$(to_hide).hide();
	});
</script>